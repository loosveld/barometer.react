var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const functions = require('firebase-functions');
const serviceAccount = require('../obs-barometer-firebase-adminsdk-xdrcv-3797900373');
const cors = require('cors')({ origin: true });
const admin = require('firebase-admin');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: `https://${process.env.GCLOUD_PROJECT}.firebaseio.com`
});
const getUserRole = function (authorization) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const tokenId = authorization.split('Bearer ')[1]; //req.get('Authorization')
            const verifiedUser = yield admin.auth().verifyIdToken(tokenId);
            const userMeta = yield admin
                .firestore()
                .doc('users/' + verifiedUser.uid)
                .get();
            return userMeta.data() ? userMeta.data().role : false;
        }
        catch (error) {
            console.error(error);
            return false;
        }
    });
};
exports.listUsers = functions.https.onRequest((req, res) => {
    cors(req, res, () => __awaiter(this, void 0, void 0, function* () {
        const userRole = yield getUserRole(req.get('Authorization'));
        userRole === 'admin' || userRole === 'superadmin'
            ? admin
                .auth()
                .listUsers(1000)
                .then(function (listUsersResult) {
                console.log('Successfully loaded users', listUsersResult);
                res.status(200).send(listUsersResult.users);
            })
            : console.error('No Permission for', userRole);
    }));
});
exports.createUser = functions.https.onRequest((req, res) => {
    cors(req, res, () => __awaiter(this, void 0, void 0, function* () {
        const userRole = yield getUserRole(req.get('Authorization'));
        delete req.body.uid;
        userRole === 'admin' || userRole === 'superadmin'
            ? admin
                .auth()
                .createUser(req.body)
                .then(function (userRecord) {
                console.log('Successfully created new user:', userRecord.uid);
                res.status(200).send(userRecord);
            })
                .catch(function (error) {
                console.error('Error updating user:', error.code);
                res.status(200).send(error.code);
            })
            : userRole
                ? console.log('No permission for ', userRole)
                : console.log('No authorization header found');
    }));
});
exports.updateUser = functions.https.onRequest((req, res) => {
    cors(req, res, () => __awaiter(this, void 0, void 0, function* () {
        const userRole = yield getUserRole(req.get('Authorization'));
        console.log(req.body);
        delete req.body.password;
        userRole === 'admin' || userRole === 'superadmin'
            ? admin
                .auth()
                .updateUser(req.body.uid, req.body)
                .then(function (userRecord) {
                // See the UserRecord reference doc for the contents of userRecord.
                console.log('Successfully updated user', userRecord.toJSON());
                res.status(200).send(userRecord);
            })
                .catch(function (error) {
                console.error('Error updating user:', error.code);
                res.send(error.code);
            })
            : userRole
                ? console.log('No permission for ', userRole)
                : console.log('No authorization header found');
    }));
});
exports.deleteUser = functions.https.onRequest((req, res) => {
    cors(req, res, () => __awaiter(this, void 0, void 0, function* () {
        const userRole = yield getUserRole(req.get('Authorization'));
        userRole === 'superadmin'
            ? admin
                .auth()
                .deleteUser(req.body.uid)
                .then(function () {
                console.log('Successfully deleted user');
                res.send('ok');
            })
                .catch(function (error) {
                console.log('Error deleting user:', error.code);
                res.send(error.code);
            })
            : userRole
                ? console.log('No permission for ', userRole)
                : console.log('No authorization header found');
    }));
});
//# sourceMappingURL=index.js.map