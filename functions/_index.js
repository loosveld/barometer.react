const functions = require('firebase-functions');
<<<<<<< HEAD:functions/index.js
var serviceAccount = require('./obs-barometer-firebase-adminsdk-xdrcv-3797900373');
=======
var serviceAccount = require('./barometer-disco-firebase-adminsdk-fdic9-35e432c130.json');
>>>>>>> bb2a4b3... general 2:functions/_index.js
var cors = require('cors')({ origin: true });
const admin = require('firebase-admin');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: `https://${process.env.GCLOUD_PROJECT}.firebaseio.com`
});

exports.listUsers = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    let userRole = await getUserRole(req.get('Authorization'));
    userRole === 'admin' || userRole === 'superadmin'
      ? admin
          .auth()
          .listUsers(1000)
          .then(function(listUsersResult) {
            console.log('Successfully loaded users', listUsersResult);
            res.status(200).send(listUsersResult.users);
          })
      : console.log('No Permission for', userRole);
  });
});

exports.createUser = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    let userRole = await getUserRole(req.get('Authorization'));
    delete req.body.uid;
    userRole === 'admin' || userRole === 'superadmin'
      ? admin
          .auth()
          .createUser(req.body)
          .then(function(userRecord) {
            console.log('Successfully created new user:', userRecord.uid);
            res.status(200).send(userRecord);
          })
          .catch(function(error) {
            console.error('Error updating user:', error.code);
            res.status(200).send(error.code);
          })
      : userRole
        ? console.log('No permission for ', userRole)
        : console.log('No authorization header found');
  });
});

exports.updateUser = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    let userRole = await getUserRole(req.get('Authorization'));
    console.log(req.body);
    delete req.body.password;
    userRole === 'admin' || userRole === 'superadmin'
      ? admin
          .auth()
          .updateUser(req.body.uid, req.body)
          .then(function(userRecord) {
            // See the UserRecord reference doc for the contents of userRecord.
            console.log('Successfully updated user', userRecord.toJSON());
            res.status(200).send(userRecord);
          })
          .catch(function(error) {
            console.error('Error updating user:', error.code);
            res.send(error.code);
          })
      : userRole
        ? console.log('No permission for ', userRole)
        : console.log('No authorization header found');
  });
});

exports.deleteUser = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    let userRole = await getUserRole(req.get('Authorization'));
    userRole === 'superadmin'
      ? admin
          .auth()
          .deleteUser(req.body.uid)
          .then(function() {
            console.log('Successfully deleted user');
            res.send('ok');
          })
          .catch(function(error) {
            console.log('Error deleting user:', error.code);
            res.send(error.code);
          })
      : userRole
        ? console.log('No permission for ', userRole)
        : console.log('No authorization header found');
  });
});

const getUserRole = async function(authorization) {
  try {
    const tokenId = authorization.split('Bearer ')[1]; //req.get('Authorization')
    const verifiedUser = await admin.auth().verifyIdToken(tokenId);
    const userMeta = await admin
      .firestore()
      .doc('users/' + verifiedUser.uid)
      .get();
    return userMeta.data() ? userMeta.data().role : false;
  } catch (error) {
    console.error(error);
    return false;
  }
};
