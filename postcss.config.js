module.exports = {
  plugins: {
    'postcss-import': {},
    'postcss-preset-env': {
      features: {
        'nesting-rules': true
      },
      browsers: ['>0.25%', 'not op_mini all']
    },
    cssnano: {}
  }
};
