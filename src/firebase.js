import firebase from '@firebase/app';
import '@firebase/firestore';
import '@firebase/auth';
import { fbConfig } from './config.js';

firebase.initializeApp(fbConfig);

export const db = firebase.firestore();
db.settings({ timestampsInSnapshots: true });
export const auth = firebase.auth();
