import { types, onSnapshot, clone, onPatch } from "mobx-state-tree";

import { ValidationStore } from "./models/Validation";
import { SurveyStore } from "./models/Survey";
import * as Alert from "./models/Alert";
import { QuestionStore } from "./models/Question";
import { AnswerStore } from "./models/Answer";
import { SchoolStore, School } from "./models/School";
import { GroupStore } from "./models/Group";
import { UserStore } from "./models/User";
import { LocaleStore } from "./models/Locale";
import ConfigStore from "./models/Config";
import { Router } from "./models/Router";

const store = types
  .model("store", {
    $locale: types.optional(LocaleStore, {}),
    $router: types.optional(Router, {}),
    $loading: types.optional(types.boolean, true),
    $alert: types.optional(Alert.store, {}),
    $user: types.optional(UserStore, {}),
    $school: types.optional(SchoolStore, {}),
    $group: types.optional(GroupStore, {}),
    $validation: types.optional(ValidationStore, {}),
    $survey: types.optional(SurveyStore, {}),
    $question: types.optional(QuestionStore, {}),
    $answer: types.optional(AnswerStore, {}),
    $config: types.optional(ConfigStore, {})
  })
  .actions(self => ({
    change(property, value) {
      self[property] = value;
    }
  }))
  .create();

onPatch(store, async patch => {
  const { $survey, $router, $user, $school, $group, $locale } = store;

  if (patch.path === "/$user/status" && patch.value === "done") {
    $locale.load();
    $group.load();
    ($router.page === "start" || $router.page === "survey") &&
      (await $survey.load($router.id));
    store.change("$loading", false);
  }
  if (patch.path === "/$router/pathname") {
    if (patch.value === "/start" || patch.value.indexOf("/survey") > -1) {
      $survey.load($router.id);
    }
    if (patch.value === "/user") {
      $user.load();
    }
    if (patch.value === "/group") {
      $group.load();
    }
    if (patch.value === "/school") {
      $school.load();
    }
  }
});

onSnapshot(store, snapshot => {
  console.log(">>>>> snapshot", snapshot);
  const { $survey, $router, $user, $group, $config } = store;

  if ($user.currentUser.isMentor && $user.status === "done") {
    if ($router.page === "" || $router.page === "start") {
      $router.push("/survey");
    }
    if ($router.page === "user") {
      $router.mode === "signin" && $router.push("/survey");
      if ($router.mode && !$user.selected && $user.status === "done") {
        $user.change(
          "selected",
          $router.key
            ? $user.list.has($router.key)
              ? clone($user.list.get($router.key))
              : $router.push("/user")
            : { _key: "new", name: "", createdBy: $user.currentUser.uid }
        );
      }
    }
    if ($router.page === "survey") {
      if (!$router.mode) $survey.setSelected(null);
      if ($router.mode && !$survey.selected && $survey.status === "done") {
        const selectedSurvey = $router.key
          ? $survey.list.has($router.key)
            ? clone($survey.list.get($router.key))
            : $router.push("/survey")
          : {
              _key: "new",
              name: "",
              createdBy: $user.currentUser.uid,
              createdOn: new Date(),
              updatedOn: new Date()
            };
        $survey.setSelected(selectedSurvey);
        // selectedSurvey.interval_period !== "none" &&
        //   $survey.setActiveIntervalPeriod();
      }
    }
    if ($router.page !== "survey") {
      $survey.setSelected(null);
    }
    if ($router.page === "group") {
      if (!$router.mode) $group.change("selected", null);
      if ($router.mode && !$group.selected && $group.status === "done") {
        $group.change(
          "selected",
          $router.key
            ? $group.list.has($router.key)
              ? clone($group.list.get($router.key))
              : $router.push("/group")
            : School.create({
                _key: "new",
                name: "",
                createdBy: $user.currentUser.uid
              })
        );
      }
    }
  }

  if (
    ($user.currentUser.isAnonymous || $user.currentUser.isAuth) &&
    !$user.currentUser.isMentor &&
    $user.status === "done"
  ) {
    if ($router.page === "") {
      $router.push($config.home);
    } else if (
      !($router.page === "start") &&
      !($router.page === "survey" && $router.mode === "view") &&
      !($router.page === "survey" && $router.mode === "results") &&
      !($router.page === "user" && $router.mode === "register") &&
      !($router.page === "user" && $router.mode === "signin")
    ) {
      $router.push("/start");
    }
    if ($router.page === "survey") {
      if (
        ($router.mode === "view" || $router.mode === "results") &&
        !$survey.selected &&
        $router.key &&
        $survey.status === "done"
      ) {
        const selected = clone($survey.list.get($router.key));
        $survey.setSelected(selected);
      }
    }
  }
});

window.store = store;

export default store;
