export const fbConfig = {
  apiKey: 'AIzaSyDLKuXiUYVPRDisg2VrDNhUqOG9SdcR8rI',
  authDomain: 'barometer-master.firebaseapp.com',
  databaseURL: 'https://barometer-master.firebaseio.com',
  projectId: 'barometer-master',
  storageBucket: 'barometer-master.appspot.com',
  messagingSenderId: '693641554257'
};

export const appConfig = {
  appName: 'BRMTR',
  templates: false,
  home: '/start',
  createNewSurvey: false,
  register_as_individual: true,
  register_as_group: true,
  register_register_title: 'Registratie',
  register_register_body:
    'Nadat we jouw emailadres hebben ontvangen mailen we je de nodige gegevens om aan de slag te gaan.',
  register_register_group_enable: 'Als school/groep invullen?',
  color1: '#348a66',
  color2: '#53bc76',
  field_types: [
    { label: 'Groep', value: 'fieldset' },
    { label: 'Titel', value: 'legend' },
    { label: 'Textveld', value: 'text' },
    { label: 'Keuzelijst', value: 'list' },
    { label: 'Ja/Nee', value: 'boolean' },
    { label: 'Verborgen', value: 'hidden' },
    {
      value: 'scale1',
      label: 'Schaal (7 punten)',
      options: [
        { label: 'Volledig oneens', value: 0 },
        { label: 'Oneens', value: 1 },
        { label: 'Eerder oneens', value: 2 },
        { label: 'Noch eens, noch oneens', value: 3 },
        { label: 'Eerder eens', value: 4 },
        { label: 'Eens', value: 5 },
        { label: 'Volledig eens', value: 6 }
      ]
    },
    {
      value: 'scale1_inverse',
      label: 'Schaal (7 punten - omgekeerd)',
      options: [
        { label: 'Volledig oneens', value: 6 },
        { label: '0neens', value: 5 },
        { label: 'Eerder oneens', value: 4 },
        { label: 'Noch eens, noch oneens', value: 3 },
        { label: 'Eerder eens', value: 2 },
        { label: 'Eens', value: 1 },
        { label: 'Volledig eens', value: 0 }
      ]
    },
    {
      value: 'scale2',
      label: 'Schaal (9 punten)',
      options: [
        { label: 'Niet', value: 0 },
        { label: '', value: 1 },
        { label: 'Weinig', value: 2 },
        { label: '', value: 3 },
        { label: 'Een beetje', value: 4 },
        { label: '', value: 5 },
        { label: 'Redelijk', value: 6 },
        { label: '', value: 7 },
        { label: 'Veel', value: 8 }
      ]
    },
    {
      value: 'scale2_inverse',
      label: 'Schaal (9 punten - omgekeerd)',
      options: [
        { label: 'Niet', value: 8 },
        { label: '', value: 7 },
        { label: 'Weinig', value: 6 },
        { label: '', value: 5 },
        { label: 'Een beetje', value: 4 },
        { label: '', value: 3 },
        { label: 'Redelijk', value: 2 },
        { label: '', value: 1 },
        { label: 'Veel', value: 0 }
      ]
    }
  ]
};
