import React from "react";
import { inject, observer } from "mobx-react";
import * as C from "vonnegut.components";

import QuestionEdit from "./question-edit";
import QuestionList from "./question-list";
//import QuestionListAnswer from './question-list-answer';

const QuestionContainer = ({ store, mode }) => {
  const { $question, $locale, $user } = store;

  return (
    <React.Fragment>
      {(mode === "edit" || mode === "new" || $question.selected) && (
        <C.ModalNew
          open={$question.selected}
          width="950px"
          height="700px"
          classNameContent="no-margin bg-gray-lightest"
          title={
            $question.selected._key === "new"
              ? "Vraag toevoegen"
              : `Vraag bewerken ${$user.currentUser.isAdmin &&
                  "-- " + $question.selected._key}`
          }
          noCloseButton
          handleClose={() => {
            $question.detach($question.selected);
            $locale.clearSelectedList();
          }}
          actions={[
            <C.ButtonNew
              key={"1"}
              className="bg-transparent"
              onClick={() => {
                $question.detach($question.selected);
                $locale.clearSelectedList();
              }}
              label={"Annuleren"}
            />,
            <C.ButtonNew
              key={"2"}
              onClick={() => {
                const validations = store.$validation.validateForm(
                  "question-edit"
                );
                console.log("validate", validations.size);
                if (validations.size === 0) {
                  $question.selected.fsSave();
                  $question.detach($question.selected);
                }
              }}
              className="bg-primary"
              label={$question.selected._key === "new" ? "Voeg toe" : "Bewaren"}
            />
          ]}
        >
          <QuestionEdit selected={$question.selected} />
        </C.ModalNew>
      )}
      {(!mode || mode === "view") && (
        <QuestionList activeQuestion={$question.activeQuestion} mode={mode} />
      )}
    </React.Fragment>
  );
};

export default inject("store")(observer(QuestionContainer));
