import React from 'react';
import * as C from 'vonnegut.components';
import { observer, inject } from 'mobx-react';

const StartDiscoAside = ({ store }) => (
  <aside className="lg-col-4 p3 flex flex-column justify-between">
    <div className="mb3 ">
      <h2 className="pointer" onClick={e => store.$router.push('/')}>
        ONTDEK JOUW UNIEKE DISCO-PROFIEL
      </h2>
      <div className="mt2">
        Diversiteitsscreening onderwijs is een meet- en reflectie-instrument dat **leerkrachten**
        hun houding en competentiegevoel inzake omgaan met diversiteit in het onderwijs in kaart
        brengt.
      </div>
    </div>
    <div className="flex">
      <C.ButtonNew
        label="Login"
        className="small bg-primary"
        onClick={() => store.$router.push('/user/signin')}
      />
    </div>
  </aside>
);

export default inject('store')(observer(StartDiscoAside));
