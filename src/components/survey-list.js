import React from 'react';
import * as C from 'vonnegut.components';
import { clone } from 'mobx-state-tree';
import * as Icon from 'react-feather';
import { values } from 'mobx';

import DialogConfirm from './_common/dialog-confirm';

import { observer, inject } from 'mobx-react';

const SurveyList = class extends React.Component {
  state = {
    confirmSurvey: false
  };
  render() {
    const { store } = this.props;
    const { $survey, $router, $user, $config } = store;
    const { confirmSurvey } = this.state;
    return (
      <div className="flex flex-wrap">
        {$user.currentUser.isCreator && (
          <div className="col-12 md-col-4 lg-col-3">
            <div className="survey-item m2" onClick={() => $router.push('/survey/new')}>
              <div className="mt4">
                <Icon.Plus className="color-primary" size={90} />
              </div>
              <div className="color-primary mb2">Nieuwe Enquete</div>
            </div>
          </div>
        )}
        {values($survey.list).map(survey => (
          <div className="col-12 md-col-4 lg-col-3" key={survey._key}>
            <div
              className="survey-item m2"
              onClick={() => $router.push('/survey/edit/' + survey._key)}
            >
              <h3 className="p2 center">{survey.name}</h3>
              <div
                className="flex flex-column items-center justify-between mb2"
                onClick={() => $router.push('/survey/edit/' + survey._key)}
              >
                <span>
                  <h2 className="color-primary">{survey.respondents.length}</h2>
                </span>
                <span>Respondenten</span>
                {$user.currentUser.isCreator && (
                  <span>
                    {survey.enabled ? (
                      <b className="color-green">Actief</b>
                    ) : (
                      <b className="color-red">Inactief</b>
                    )}
                  </span>
                )}
              </div>

              {$user.currentUser.isCreator && (
                <div className="bg-gray-lightest width-full actions ">
                  {$config.templates &&
                    $user.currentUser.isCreator &&
                    survey.isTemplate && (
                      <div className="bg-primary width-full center color-white">
                        <span className="p2">SJABLOON</span>
                      </div>
                    )}
                  <div className="flex justify-between">
                    <C.ButtonNew
                      className="bg-transparent no-label"
                      onClick={e => {
                        e.stopPropagation();
                        $router.push('/survey/edit/' + survey._key);
                        $survey.setSelected(clone($survey.list.get(survey._key)));
                      }}
                    >
                      {$user.currentUser.isCreator ? (
                        <Icon.Edit2 size={18} color="#444" />
                      ) : (
                        <Icon.List size={18} color="#444" />
                      )}
                    </C.ButtonNew>

                    <C.ButtonNew
                      className="bg-transparent no-label"
                      onClick={e => {
                        e.stopPropagation();
                        $router.push('/survey/settings/' + survey._key);
                        $survey.setSelected(clone($survey.list.get(survey._key)));
                      }}
                    >
                      <Icon.Settings size={18} color="#444" />
                    </C.ButtonNew>

                    <C.ButtonNew
                      className="bg-transparent no-label"
                      onClick={e => {
                        e.stopPropagation();
                        $router.push('/survey/share/' + survey._key);
                        $survey.setSelected(clone($survey.list.get(survey._key)));
                      }}
                    >
                      <Icon.Share2 size={18} color="#444" />
                    </C.ButtonNew>

                    <C.ButtonNew
                      className="bg-transparent no-label"
                      onClick={e => {
                        e.stopPropagation();
                        $router.push('/survey/results/' + survey._key);
                        $survey.setSelected(clone($survey.list.get(survey._key)));
                      }}
                    >
                      <Icon.BarChart2 size={18} color="#444" />
                    </C.ButtonNew>
                    {$user.currentUser.isCreator && (
                      <C.ButtonNew
                        className="bg-transparent no-label"
                        onClick={e => {
                          e.stopPropagation();
                          this.setState({ confirmSurvey: survey });
                        }}
                      >
                        <Icon.Trash size={18} color="#444" />
                      </C.ButtonNew>
                    )}
                  </div>
                </div>
              )}
            </div>
          </div>
        ))}
        <DialogConfirm
          text={
            'Let op! Deze enquête wordt niet verwijderd maar op inactief geplaatst, deze enquête wordt onzichtbaar voor gewone gebruikers.'
          }
          open={confirmSurvey}
          labelSubmit="Inactief maken"
          onSubmit={() => {
            $survey.list.get(confirmSurvey._key).change('enabled', false);
            $survey.list.get(confirmSurvey._key).fsSave();
            //$survey.list.get(confirmSurvey._key).fsDelete();
            this.setState({ confirmSurvey: false });
          }}
          onClose={() => this.setState({ confirmSurvey: false })}
        />
      </div>
    );
  }
};

export default inject('store')(observer(SurveyList));
