import React from "react";
import * as C from "vonnegut.components";
import { observer, inject } from "mobx-react";
import * as Icon from "react-feather";

const QuestionListItem = class extends React.Component {
  state = {
    inputOrder: this.props.question.order
  };
  render() {
    const { question, store } = this.props;
    const {
      $question,
      $locale,
      $answer,
      $survey,
      $validation,
      $router,
      $user,
      $config
    } = store;

    const name = $locale.t(question._key);
    const description = $locale.t(question._key + "_description");
    const appConfigSelectedType = $config.field_types.find(
      type => type.value === question.type
    );

    return (
      <div
        className={`question-content relative mb2 ${
          question.type === "fieldset" ? "legend" : question.type
        }`}
        id={question._key}
      >
        {question.type === "fieldset" && <h2>{name}</h2>}

        {question.type === "list" && (
          <C.Select
            options={question.typeOptions.toJSON()}
            label={name}
            defaultValue={question.answerTemp && question.answerTemp.value}
            name={question._key}
            disabled={$router.mode !== "view"}
            required={question.required}
            onChange={e => $answer.setToTempList(e, question)}
          />
        )}

        {question.type === "legend" && <h5>{name}</h5>}

        {question.type === "boolean" && (
          <C.RadioGroup
            label={name}
            required={question.required}
            validation={{
              label: $locale.t(
                $validation.getValidationKey(
                  question._key,
                  "survey-" + $survey.selected._key
                )
              )
            }}
          >
            <C.Radio
              value="ja"
              label={"Ja"}
              className="mr2"
              type="radio"
              required={question.required}
              disabled={$router.mode !== "view"}
              onChange={e => $answer.setToTempList(e, question)}
              checked={question.answerTemp && question.answerTemp.value}
              name={question._key}
            />
            <C.Radio
              label={"Nee"}
              value="nee"
              type="radio"
              required={question.required}
              disabled={$router.mode !== "view"}
              onChange={e => $answer.setToTempList(e, question)}
              checked={question.answerTemp && question.answerTemp.value}
              name={question._key}
            />
          </C.RadioGroup>
        )}

        {(question.type === "text" ||
          question.type === "email" ||
          question.type === "number" ||
          question.type === "date") && (
          <C.Input
            label={name}
            type={question.type}
            disabled={$router.mode !== "view"}
            value={question.answerTemp && question.answerTemp.value}
            name={question._key}
            onChange={e => $answer.setToTempList(e, question)}
            required={question.required}
            validation={{
              label: $validation.getValidationKey(
                question._key,
                "survey-" + $survey.selected._key
              )
            }}
          />
        )}

        {question.type.indexOf("scale") > -1 && (
          <C.RadioGroup
            label={name}
            className="width-full"
            required={question.required}
            validation={{
              label: $locale.t(
                $validation.getValidationKey(
                  question._key,
                  "survey-" + $survey.selected._key
                )
              )
            }}
          >
            <div className="line flex justify-between width-full relative">
              {appConfigSelectedType &&
                appConfigSelectedType.options.map((option, i) => (
                  <C.Radio
                    key={option.value}
                    type="radio"
                    label={option.label}
                    value={option.value}
                    disabled={$router.mode !== "view"}
                    required={question.required}
                    name={question._key}
                    checked={
                      $router.mode === "view" &&
                      question.answerTemp &&
                      String(question.answerTemp.value) === String(option.value)
                    }
                    className={`scale-item column ${option.key} ${
                      appConfigSelectedType.options.filter(o => o.key !== "nvt")
                        .length ===
                      i + 1
                        ? "last"
                        : ""
                    }`}
                    onChange={e => $answer.setToTempList(e, question)}
                  />
                ))}
            </div>
          </C.RadioGroup>
        )}

        {question.type === "hidden" && (
          <C.Input
            label={name + " (verborgen)"}
            disabled={true}
            value={question.answerTemp && (question.answerTemp.value || "")}
            name={question._key}
          />
        )}

        {description && <div className="description mt2">{description}</div>}

        {$router.mode !== "view" &&
          question.isEditable &&
          $user.currentUser.isCreator && (
            <div
              className="flex bg-gray-lightest absolute "
              style={{
                right: "0px",
                top: "0px"
              }}
            >
              <C.Input
                value={this.state.inputOrder}
                onChange={e => {
                  this.setState({ inputOrder: e.target.value });
                }}
                onKeyDown={e => {
                  if (e.key === "Enter") {
                    question.change("order", Number(e.target.value));
                    question.fsSave();
                  }
                }}
                onBlur={e => {
                  this.setState({ inputOrder: question.order });
                }}
                type="text"
                style={{
                  width: "65px"
                }}
              />
              <C.ButtonNew
                className="bg-transparent no-label small"
                onClick={() => $question.setSelected(question)}
              >
                <Icon.Edit2 size={18} />
              </C.ButtonNew>
              <C.ButtonNew
                className="bg-transparent no-label small"
                onClick={async () => {
                  await question.fsDelete();
                  $question.deleteFromList(question._key);
                }}
              >
                <Icon.Trash size={18} />
              </C.ButtonNew>
            </div>
          )}
      </div>
    );
  }
};

export default inject("store")(observer(QuestionListItem));
