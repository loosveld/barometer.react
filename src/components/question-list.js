import React, { Component } from "react";
import * as C from "vonnegut.components";
import QuestionListItem from "./question-list-item";
import * as Icon from "react-feather";

import { observer, inject } from "mobx-react";
import { isValid } from "date-fns";

const QuestionList = class extends Component {
  state = {
    confirmSurvey: false
  };
  isValid = key => {
    let count = 0;
    let question = this.props.store.$question.list.get(key);
    question.childrenRecursive &&
      question.childrenRecursive.forEach(child => {
        const field = document.querySelector("#\\" + child._key + " input");
        field && this.props.store.$validation.getValidityKey(field) && count++;
      });

    console.log("count :", count, key);
    return count ? false : true;
  };
  render() {
    const { mode, store } = this.props;
    const { $question, $survey, $alert, $validation, $user, $locale } = store;

    return (
      <React.Fragment>
        {$validation.list.size > 0 && (
          <div className="bg-red color-white p2 border-radius mb2">
            {$locale.t("validation_globaleMessage")}
          </div>
        )}
        {$survey.selected.questionTree.map((q1, i) => (
          <div
            key={q1._key}
            className={`question-${q1.type} mb2 ${q1._key} ${
              mode !== "view" ||
              q1._key ===
                $survey.selected.getActiveQuestionKey(
                  $survey.selected._activeQuestion
                )
                ? "block"
                : "display-none"
            }`}
          >
            <QuestionListItem question={$question.list.get(q1._key)} />
            {q1.type === "fieldset" &&
              q1.children.map(q2 => (
                <div
                  key={q2._key}
                  className={`question-${q2.type} mb2 ${q2._key}`}
                >
                  <QuestionListItem question={$question.list.get(q2._key)} />
                  {q2.type === "fieldset" &&
                    q2.children.map(q3 => (
                      <div
                        key={q3._key}
                        className={`question-${q3.type} mb2 ${q2._key}`}
                      >
                        <QuestionListItem
                          question={$question.list.get(q3._key)}
                        />
                      </div>
                    ))}
                  {$user.currentUser.isCreator &&
                    q2.type === "fieldset" &&
                    mode !== "view" && (
                      <NewActions
                        {...this.props.store}
                        noGroup
                        parentKey={q2._key}
                      />
                    )}
                </div>
              ))}
            {mode !== "view" &&
              $user.currentUser.isCreator && (
                <NewActions {...this.props.store} parentKey={q1._key} />
              )}
          </div>
        ))}

        {mode !== "view" &&
          $user.currentUser.isCreator && <NewActions {...this.props.store} />}

        {$validation.list.size > 0 && (
          <div className="bg-red color-white p2 border-radius">
            {$locale.t("validation_globaleMessage")}
          </div>
        )}

        {mode === "view" && (
          <div className="flex justify-between mt3">
            <C.ButtonNew
              label={$locale.t("previous")}
              icon={<Icon.ArrowLeft />}
              className="bg-primary prev"
              disabled={!$survey.selected._activeQuestion}
              onClick={$survey.selected.prevActiveQuestion}
            />
            <div className="flex items-center">
              {$survey.selected.questionTree.map((q, i) => (
                <span
                  key={i}
                  onClick={() => {
                    window.scrollTo(0, 0);
                    $survey.selected._activeQuestion > i &&
                      $survey.selected.change("_activeQuestion", i);
                  }}
                  className={`step ${$survey.selected._activeQuestion === i &&
                    "active"} ${$survey.selected._activeQuestion > i &&
                    "completed"} ${$survey.selected._activeQuestion > i &&
                    "pointer"} ${
                    $survey.selected._activeQuestion > i &&
                    !this.isValid(q._key)
                      ? "invalid"
                      : ""
                  }`}
                >
                  {!this.isValid(q._key) && $survey.selected._activeQuestion > i
                    ? "!"
                    : i + 1}
                </span>
              ))}
            </div>
            <C.ButtonNew
              label={$locale.t("next")}
              iconRight={<Icon.ArrowRight />}
              visible={$survey.selected.getActiveQuestionKey(
                $survey.selected._activeQuestion + 1
              )}
              className={`bg-primary next`}
              onClick={$survey.selected.nextActiveQuestion}
            />
            <C.ButtonNew
              className="bg-blue"
              disabled={$alert.list.size > 0 || $validation.list.size > 0}
              visible={
                !$survey.selected.getActiveQuestionKey(
                  $survey.selected._activeQuestion + 1
                )
              }
              onClick={$survey.selected.submitAnswers}
            >
              Versturen
            </C.ButtonNew>
          </div>
        )}
      </React.Fragment>
    );
  }
};

const NewActions = ({
  $question,
  $survey,
  $user,
  $locale,
  parentKey,
  noGroup
}) => {
  return (
    <React.Fragment>
      <C.ButtonNew
        label="Nieuwe Vraag"
        onClick={() => {
          $locale.clearSelectedList();
          $question.setSelected({
            _key: "new",
            surveyKey: $survey.selected._key,
            createdBy: $user.currentUser.uid,
            parentKey,
            order: $survey.selected.getHighestOrderValue(parentKey)
          });
        }}
        className="mr2 bg-primary"
      />
      {!noGroup && (
        <C.ButtonNew
          label="Nieuwe Groep"
          className="mr2 bg-primary"
          onClick={() =>
            $question.setSelected({
              _key: "new",
              type: "fieldset",
              surveyKey: $survey.selected._key,
              createdBy: $user.currentUser.uid,
              parentKey,
              order: $survey.selected.getHighestOrderValue(parentKey)
            })
          }
        />
      )}
    </React.Fragment>
  );
};

export default inject("store")(observer(QuestionList));
