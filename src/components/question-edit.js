import React, { Component } from "react";
import * as C from "vonnegut.components";
import { observer, inject } from "mobx-react";
import { Question } from "../models/Question";
import * as Icon from "react-feather";

import { entries } from "mobx";

const QuestionEdit = class extends Component {
  state = {
    logic: false
  };

  componentDidUpdate = prevProps => {
    if (document.activeElement.name === "addRow") {
      document.activeElement.value = "";
      var strLength = this.latestRowRef.value.length;
      this.latestRowRef && this.latestRowRef.focus();
      this.latestRowRef &&
        this.latestRowRef.setSelectionRange(strLength, strLength);
    }
  };
  render() {
    const {
      $question,
      $validation,
      $survey,
      $locale,
      $user,
      $config
    } = this.props.store;

    const appConfigSelectedType = $config.field_types.find(
      type => type.value === $question.selected.type
    );

    return (
      <C.Form name="question-edit">
        {Object.keys(Question.properties).map((prop, i) => (
          <React.Fragment key={"prop" + i}>
            {prop === "parentKey" &&
              $user.currentUser.isAdmin && (
                <div className="question-edit-prop prop-parentkey">
                  <C.Input
                    label="Parentkey"
                    onChange={e =>
                      $question.selected.change([prop], e.target.value)
                    }
                    value={$question.selected.parentKey || ""}
                  />
                </div>
              )}
            {prop === "name" && (
              <div className="question-edit-prop prop-name">
                {$survey.selected.languages.split(",").map(
                  (language, i) =>
                    language && (
                      <div className="flex mxn1 " key={language}>
                        <C.Input
                          label={i === 0 && "Taal"}
                          value={language.substring(0, 2)}
                          disabled
                          className="mx1"
                        />
                        <C.Input
                          label={i === 0 && prop}
                          name={prop}
                          className={`mb1 col-11 mx1`}
                          value={$locale.t($question.selected._key, language)} //normaal ist met +prop ma als er geen prop is, dan ist name want alles zit nu zo in db
                          onChange={e =>
                            $locale.setSelected(
                              $question.selected._key,
                              language,
                              e.target.value
                            )
                          }
                          validation={{
                            label: $validation.getValidationKey(
                              prop + "_" + language,
                              "question-edit"
                            )
                          }}
                        />
                      </div>
                    )
                )}
              </div>
            )}
            {prop === "required" &&
              $question.selected.type !== "fieldset" && (
                <div
                  key={"prop" + i}
                  className={
                    `question-edit-prop prop-type type-` +
                    $question.selected.type
                  }
                >
                  <C.CheckboxNew
                    label="Verplicht"
                    className="row"
                    name="required"
                    checked={$question.selected.required}
                    onChange={e => {
                      console.log("e :", e.target.name, e.target.checked);
                      $question.selected.change(
                        e.target.name,
                        e.target.checked ? true : false
                      );
                    }}
                  />
                </div>
              )}
            {prop === "type" && (
              <div
                key={"prop" + i}
                className={
                  `question-edit-prop prop-type mb2 type-` +
                  $question.selected.type
                }
              >
                <C.Select
                  label={prop}
                  className={"mb2"}
                  name="Type"
                  disabled={$question.selected.answers.length > 0}
                  defaultValue={$question.selected.type || "select"}
                  required
                  options={$config.field_types}
                  onChange={e =>
                    $question.selected.change([prop], e.target.value)
                  }
                  validation={{
                    label: $validation.getValidationKey("type", "question-edit")
                  }}
                />
              </div>
            )}
            {prop === "typeOptions" &&
              $question.selected.type === "list" && (
                <div
                  key={"prop" + i}
                  className="question-edit-prop prop-typeOptions type-list"
                >
                  {$question.selected.typeOptions.map((option, i) => (
                    <div className="flex" key={"option" + i}>
                      <C.Input
                        label={i === 0 && "Label"}
                        className={"mb1 bg-gray-lightest width-full mr1"}
                        required
                        name={"option" + i}
                        value={option.label}
                        validation={{
                          label: $validation.getValidationKey(
                            "option" + i,
                            "question-edit"
                          )
                        }}
                        handleRef={input => {
                          if (
                            $question.selected &&
                            i === $question.selected.typeOptions.length - 1
                          )
                            this.latestRowRef = input;
                        }}
                        onChange={e => option.change(e.target.value, "label")}
                      />
                      <C.Input
                        label={i === 0 && "Waarde"}
                        className={"mb1 bg-gray-lightest"}
                        required
                        name={"option" + i}
                        value={option.value}
                        validation={{
                          label: $validation.getValidationKey(
                            "option" + i,
                            "question-edit"
                          )
                        }}
                        onChange={e => option.change(e.target.value, "value")}
                      />
                    </div>
                  ))}
                  <C.Input
                    placeholder="Keuze toevoegen"
                    name="addRow"
                    onChange={e =>
                      $question.selected.addToTypeOptions(e.target.value)
                    }
                  />
                </div>
              )}
            {prop === "typeOptions" &&
              $question.selected.type.indexOf("scale") > -1 && (
                <div
                  key={"prop" + i}
                  className="question-edit-prop prop-scale-options type-scale flex justify-between"
                >
                  {appConfigSelectedType &&
                    appConfigSelectedType.options.map(option => (
                      <div key={option.value}>
                        {option.label} ({option.value})
                      </div>
                    ))}
                </div>
              )}
            {prop === "order" && (
              <div key={"prop" + i} className="question-edit-prop prop-order">
                <C.Input
                  label={"Volgorde"}
                  type="number"
                  className={"mb2"}
                  name={prop}
                  required
                  value={$question.selected && $question.selected[prop]}
                  onChange={e =>
                    $question.selected.change([prop], Number(e.target.value))
                  }
                  validation={{
                    label: $validation.getValidationKey(prop, "question-edit")
                  }}
                />
              </div>
            )}
            {prop === "description" && (
              <div
                key={"prop" + i}
                className="question-edit-prop prop-description "
              >
                <label className="mb1">Omschrijving</label>
                {$survey.selected.languages.split(",").map(
                  (language, i) =>
                    language && (
                      <div
                        key={language}
                        className={`flex mxn1 ${i + 1 !==
                          $survey.selected.languages.length && "mb1"}`}
                      >
                        <C.Input
                          value={language.substring(0, 2)}
                          disabled
                          className="mx1"
                        />
                        <C.Textarea
                          wysiwyg={false}
                          className="width-full mx1"
                          value={$locale.t(
                            $question.selected._key + "_description",
                            language
                          )}
                          onChange={e =>
                            $locale.setSelected(
                              $question.selected._key + "_description",
                              language,
                              e.target.value
                            )
                          }
                        />
                      </div>
                    )
                )}
              </div>
            )}

            {prop === "feedback" && (
              <div
                key={"prop" + i}
                className="question-edit-prop prop-feedback "
              >
                <label className="mb1">Feedback</label>
                {$survey.selected.languages.split(",").map(
                  (language, i) =>
                    language && (
                      <div
                        key={language}
                        className={`flex mxn1 ${i + 1 !==
                          $survey.selected.languages.length && "mb1"}`}
                      >
                        <C.Input
                          value={language.substring(0, 2)}
                          disabled
                          className="mx1"
                        />
                        <C.Textarea
                          wysiwyg={false}
                          className="width-full mx1"
                          value={$locale.t(
                            $question.selected._key + "_feedback",
                            language
                          )}
                          onChange={e =>
                            $locale.setSelected(
                              $question.selected._key + "_feedback",
                              language,
                              e.target.value
                            )
                          }
                        />
                      </div>
                    )
                )}
              </div>
            )}
          </React.Fragment>
        ))}

        {$question.selected.type !== "fieldset" && (
          <div className="question-edit-prop">
            <C.CheckboxNew
              label="Als/Dan"
              className="row"
              checked={$question.selected.logic}
              onChange={$question.selected.initLogic}
            />

            {$question.selected.logic && (
              <div className="bg-gray-lightest">
                {entries($question.selected.logic.conditions).map(
                  (condition, i) => (
                    <React.Fragment key={"condition" + i}>
                      <div className="flex mxn1" id={condition[0]}>
                        <C.Input
                          label="Als"
                          value={condition[1].question}
                          name={"question"}
                          className="mt1 row col-10 ml1"
                          disabled={true}
                        />

                        <C.Select
                          options={[
                            {
                              value: "is",
                              label: "is gelijk aan..."
                            },
                            {
                              value: "not",
                              label: "niet gelijk aan..."
                            }
                          ]}
                          value={condition[1].condition}
                          name={"condition"}
                          className="mt1 row col-2 ml1"
                          onChange={condition[1].change}
                        />
                      </div>
                      <div className="flex mxn1">
                        <C.Input
                          value={condition[1].answer}
                          name={"answer"}
                          className="col-10 ml1 mt1 row"
                          label="."
                          onChange={condition[1].change}
                        />
                        <div className="col-2 ml1 mt1 ">
                          {$question.selected.logic.conditions.size > 1 && (
                            <C.ButtonNew
                              icon={<Icon.Minus />}
                              type="button"
                              onClick={() =>
                                $question.selected.logic.deleteFromConditionList(
                                  condition[0]
                                )
                              }
                              className="bg-transparent"
                            />
                          )}
                        </div>
                      </div>
                    </React.Fragment>
                  )
                )}
                <div
                  className="flex mxn2 mt2"
                  style={{ borderTop: "1px dashed gray" }}
                >
                  <div className="col-10 ml2 mt2">
                    <C.Select
                      label="Actie"
                      options={$survey.selected.questions.map(question => ({
                        value: "hide_" + question._key,
                        label: "Verberg: " + $locale.t(question._key)
                      }))}
                      name="action"
                      className="row"
                      value={$question.selected.logic.action}
                      onChange={$question.selected.logic.change}
                    />
                  </div>
                </div>
              </div>
            )}
          </div>
        )}
      </C.Form>
    );
  }
};

/* <C.ButtonNew
      icon={<Icon.Plus />}
      type="button"
      onClick={() =>
        $question.selected.logic.setToConditionList(Date.now(), {
          question: '',
          condition: 'is',
          answer: ''
        })
      }
      className="bg-transparent"
    /> */

export default inject("store")(observer(QuestionEdit));
