import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import * as C from 'vonnegut.components';
import * as Icon from 'react-feather';
import GroupEdit from './group-edit.js';
import { values } from 'mobx';

const GroupContainer = class extends Component {
  cols = [
    { label: 'Naam', key: 'name', width: '40%' },
    { label: 'CreatedBy', key: 'createdBy', width: '30%' },
    { label: 'CreatedOn', key: 'createdOn', width: '30%', type: 'date' },
    {
      type: 'action',
      icon: <Icon.Edit2 />,
      className: 'bg-transparent',
      onClick: (e, row) => this.props.store.$router.push('/group/edit/' + row._key)
    }
  ];
  render() {
    const { $group, $router, $validation } = this.props.store;
    return (
      <React.Fragment>
        <header className="flex justify-between">
          <h2>Groepen/scholen</h2>
        </header>
        <div className="p3">
          {$group.selected &&
            $router.mode === 'edit' && (
              <C.ModalNew
                open={$group.selected}
                width="950px"
                height="700px"
                classNameContent="no-margin bg-gray-lightest p3"
                title={$group.selected._key === 'new' ? 'Group toevoegen' : $group.selected.name}
                noCloseButton
                handleClose={() => {
                  $group.detach($group.selected);
                }}
                actions={[
                  <C.ButtonNew
                    key={'1'}
                    className="bg-transparent"
                    onClick={() => {
                      this.props.store.$router.push('/group');
                      $group.detach($group.selected);
                    }}
                    label={'Annuleren'}
                  />,
                  <C.ButtonNew
                    key={'2'}
                    onClick={() => {
                      const validations = $validation.validateForm('group-edit');
                      if (validations.size === 0) {
                        $group.selected.fsSave();
                        $group.detach($group.selected);
                      }
                    }}
                    label={$group.selected._key === 'new' ? 'Voeg toe' : 'Bewaren'}
                  />
                ]}
              >
                <GroupEdit {...this.props} />
              </C.ModalNew>
            )}

          <div>
            <C.Search onChange={target => false} name="filter-search" />
            <div className="mt2 mb2">
              <b>{$group.list.size > 0 && $group.list.size}</b>
              &nbsp;scholen/groepen gevonden
            </div>
            <C.List cols={this.cols} className="zebra">
              {values($group.list).map(group => (
                <C.ListRow
                  key={group._key}
                  row={group}
                  onRowClick={(e, row) => this.props.store.$router.push('/group/edit/' + row._key)}
                />
              ))}
            </C.List>
          </div>
        </div>
      </React.Fragment>
    );
  }
};

export default inject('store')(observer(GroupContainer));
