import React from 'react';
import * as C from 'vonnegut.components';
import { observer, inject } from 'mobx-react';
import { values } from 'mobx';
import { clone } from 'mobx-state-tree';

import StartDiscoAside from './start-disco-aside';
import * as Icon from 'react-feather';
import ReactMarkdown from 'react-markdown';

const StartDisco = ({ store }) => (
  <div className="flex" style={{ minHeight: '500px' }}>
    <StartDiscoAside />
    <section className="lg-col-8 relative">
      <div className="absolute" style={{ right: '2rem', top: '2rem' }}>
        {store.$user.currentUser.uid}
      </div>
      <div className="flex mxn2 ">
        {values(store.$survey.list).map(survey => (
          <div className="lg-col-6 mx2 bg-white border-radius-large">
            <div class="height-full m2  flex flex-column justify-between">
              <ReactMarkdown source={store.$locale.t(survey._key + 'introText')} />
              <C.ButtonNew
                className="bg-primary medium"
                onClick={e => {
                  store.$router.push('/start/disco/?survey=' + survey._key);
                  if (!store.$user.currentUser.uid) {
                    store.$user.change('registerEnabled', true);
                  } else {
                    store.$survey.change('selected', clone(store.$survey.list.get(survey._key)));
                    store.$survey.selected.change('_disableWelcome', true);
                  }
                }}
                icon={<Icon.Play />}
                label="START"
              />
            </div>
          </div>
        ))}
      </div>
      <div class="mt3">
        * Het invullen van DISCO neemt gemiddeld een 20-tal minuten in beslag. Afhankelijk van hoe
        lang je stilstaat bij elke stelling, kan dit natuurlijk iets langer of korter duren.{' '}
      </div>
    </section>
  </div>
);

export default inject('store')(observer(StartDisco));
