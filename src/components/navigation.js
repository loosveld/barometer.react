import React from "react";
import { inject, observer } from "mobx-react";
import * as C from "vonnegut.components";
import * as Icon from "react-feather";

const Navigation = ({ type, store }) => {
  const { $survey, $router, $user, $config } = store;
  const { page, mode, key } = $router;

  return (
    <aside className="main flex ">
      <div className="flex flex-column justify-between level-1">
        <div>
          <C.ButtonNew
            style={{ height: "100px" }}
            onClick={() => $router.push("/survey")}
            className={["mb2", "logo", "bg-primary", "no-label"]}
          >
            <span className="color-white h2">{$config.appName}</span>
          </C.ButtonNew>
          <C.ButtonNew
            onClick={() => $router.push("/survey")}
            className={
              page === "survey"
                ? ["bg-transparent", "no-label", "active"]
                : ["bg-transparent", "no-label"]
            }
          >
            <Icon.Grid />
          </C.ButtonNew>
          {$user.currentUser.isAdmin && (
            <C.ButtonNew
              onClick={() => $router.push("/user")}
              className={
                page === "user"
                  ? ["bg-transparent", "no-label", "active"]
                  : ["bg-transparent", "no-label"]
              }
            >
              <Icon.Users />
            </C.ButtonNew>
          )}

          {$user.currentUser.isCreator && (
            <C.ButtonNew
              onClick={() => $router.push("/group")}
              className={
                page === "group"
                  ? ["bg-transparent", "no-label", "active"]
                  : ["bg-transparent", "no-label"]
              }
            >
              <Icon.Box />
            </C.ButtonNew>
          )}
        </div>
        <div className="flex flex-column">
          <C.ButtonNew
            onClick={() => $router.push("/user/edit/" + $user.currentUser.uid)}
            className={
              mode === "results"
                ? ["bg-transparent", "no-label", "active"]
                : ["bg-transparent", "no-label"]
            }
          >
            <Icon.User />
          </C.ButtonNew>
          <C.ButtonNew
            onClick={$user.signOut}
            className={["bg-transparent", "no-label"]}
          >
            <Icon.LogOut />
          </C.ButtonNew>
        </div>
      </div>
      <div className="flex flex-column justify-between p1 width-full">
        {$survey.selected &&
          key && (
            <nav className="flex flex-column justify-start height-full">
              <C.ButtonNew
                onClick={() =>
                  $router.push("/survey/edit/" + $survey.selected._key)
                }
                className={
                  mode === "edit"
                    ? ["bg-transparent", "active"]
                    : ["bg-transparent"]
                }
              >
                <Icon.Edit />
                <span>Vragen</span>
              </C.ButtonNew>
              {$user.currentUser.isCreator && (
                <C.ButtonNew
                  onClick={() =>
                    $router.push("/survey/settings/" + $survey.selected._key)
                  }
                  className={
                    mode === "settings"
                      ? ["bg-transparent", "active"]
                      : ["bg-transparent"]
                  }
                >
                  <Icon.Settings />
                  <span>Instellingen</span>
                </C.ButtonNew>
              )}
              <C.ButtonNew
                onClick={() =>
                  $router.push("/survey/share/" + $survey.selected._key)
                }
                className={
                  mode === "share"
                    ? ["bg-transparent", "active"]
                    : ["bg-transparent"]
                }
              >
                <Icon.Share2 />
                <span>Delen</span>
              </C.ButtonNew>
              {(($user.currentUser.isMentor &&
                !$config.results_hide_for_mentor) ||
                $user.currentUser.isCreator) && (
                <C.ButtonNew
                  onClick={() =>
                    $router.push("/survey/results/" + $survey.selected._key)
                  }
                  className={
                    mode === "results"
                      ? ["bg-transparent", "active"]
                      : ["bg-transparent"]
                  }
                >
                  <Icon.PieChart />
                  <span>Resultaten</span>
                </C.ButtonNew>
              )}
            </nav>
          )}
      </div>
    </aside>
  );
};

export default inject("store")(observer(Navigation));
