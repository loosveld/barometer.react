import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';

import Alert from './_common/alert';
import SurveyContainer from './survey';
import UserContainer from './user';
import GroupContainer from './group';
import StartContainer from './start';
import Navigation from './navigation';
import * as C from 'vonnegut.components';
import UserRegister from './user-register';

class App extends Component {
  render() {
    const { $router, $user, $loading, $config, $locale } = this.props.store;
    return (
      <div
        className={`${$config.id} ${$router.page ? 'page-' + $router.page : ''} ${
          $router.mode ? 'mode-' + $router.mode : ''
        } ${$user.currentUser.isMentor ? 'private' : 'public'}`}
      >
        <Alert />
        {!$loading ? (
          <React.Fragment>
            {$user.currentUser.isMentor && <Navigation type="main" />}
            <main>
              {$router.page === 'survey' && <SurveyContainer />}
              {$router.page === 'user' && <UserContainer />}
              {$router.page === 'group' && <GroupContainer />}
              {$router.page === 'start' && <StartContainer />}
            </main>
            <UserRegister />
            {!$user.currentUser.isMentor && (
              <a
                className="flex pt2 pb4 color-white justify-center block copyright"
                href="https://www.barometer-app.be"
                target="blank"
              >
                {$locale.t('barometer_app_link_label')}
              </a>
            )}
          </React.Fragment>
        ) : (
          <C.Loader loading="true" />
        )}
      </div>
    );
  }
}

export default inject('store')(observer(App));
