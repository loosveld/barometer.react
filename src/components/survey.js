import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import * as C from 'vonnegut.components';
import LanguageSwitcher from './_common/language-switcher';
import * as dateFns from 'date-fns';

import { clone } from 'mobx-state-tree';

import SurveyEdit from './survey-edit';
import SurveySettings from './survey-settings';
import SurveyShare from './survey-share';
import SurveyView from './survey-view';
import SurveyList from './survey-list';
import SurveyResults from './survey-results';

const SurveyContainer = class extends Component {
  handleSubmitNew = async () => {
    const { $survey, $validation, $router } = this.props.store;
    let docRef = false;
    let validations = $validation.validateForm('new-survey');
    if (validations.size === 0) docRef = await $survey.selected.fsSave();
    $survey.destroy($survey.selected);
    $survey.setSelected(clone($survey.list.get(docRef.id)));
    if (docRef) $router.push('/survey/edit/' + $survey.selected._key);
  };

  render() {
    const { $survey, $router, $user } = this.props.store;

    return $user.currentUser.isMentor ? (
      <React.Fragment>
        {!$user.currentUser.isCreator && (
          <header className="flex justify-between">
            <h2>{$survey.selected ? $survey.selected.name : 'Beschikbare Enquêtes'}</h2>
          </header>
        )}

        {$user.currentUser.isCreator &&
          $survey.selected && (
            <header className="flex justify-between">
              <div>
                <h2>{$survey.selected && $survey.selected.name}</h2>
              </div>
              <div className="color-gray-light font-size-small right-align">
                Laatst aangepast
                <br />
                door {$survey.selected.updatedBy}
                <br />
                op {dateFns.format($survey.selected.updatedOn, 'DD/MM/YY HH:MM')}
              </div>
            </header>
          )}

        <div className="p3">
          {$router.mode === 'view' && <SurveyView {...this.props} />}
          {$router.mode === 'edit' && <SurveyEdit {...this.props} />}
          {$router.mode === 'settings' && <SurveySettings mode={$router.mode} />}
          {$router.mode === 'share' && <SurveyShare {...this.props} />}
          {$router.mode === 'results' && <SurveyResults {...this.props} />}
          {($router.mode === 'new' || (!$router.mode && $survey.selected)) && (
            <C.ModalNew
              open={$survey.selected}
              classNameContent="no-margin-top no-margin-bottom "
              width="600px"
              height="500px"
              title={'Enquête toevoegen'}
              noCloseButton
              handleClose={() => $router.push('/survey')}
              actions={[
                <C.ButtonNew
                  onClick={() => $router.push('/survey')}
                  label={'Annuleren'}
                  className={'bg-transparent'}
                />,
                <C.ButtonNew
                  onClick={this.handleSubmitNew}
                  label={'Voeg toe'}
                  className={'bg-primary'}
                />
              ]}
            >
              <SurveySettings mode={'new'} />
            </C.ModalNew>
          )}
          {(!$router.mode || $router.mode === 'new') && <SurveyList />}
        </div>
      </React.Fragment>
    ) : (
      $survey.selected && (
        <div className="p3">
          {<LanguageSwitcher />}
          {$router.mode === 'view' && <SurveyView {...this.props} />}
          {$router.mode === 'results' && <SurveyResults {...this.props} />}
        </div>
      )
    );
  }
};

export default inject('store')(observer(SurveyContainer));
