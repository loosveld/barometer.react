import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import * as C from 'vonnegut.components';

const GroupEdit = class extends Component {
  render() {
    const { $group } = this.props.store;
    return (
      <C.Form name="group-edit">
        {$group.selected &&
          Object.keys($group.selected.$treenode.type.properties).map(prop => (
            <div className={{ padding: '1rem 2rem', borderBottom: '1px solid #ddd' }}>
              <C.Input
                label={prop}
                value={$group.selected[prop]}
                onChange={e => $group.selected.change([prop], e.target.value)}
              />
            </div>
          ))}
      </C.Form>
    );
  }
};

export default inject('store')(observer(GroupEdit));
