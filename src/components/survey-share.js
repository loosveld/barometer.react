import React, { Component } from "react";
import { clone } from "mobx-state-tree";
import * as C from "vonnegut.components";
import { observer } from "mobx-react";

const SurveyShare = class extends Component {
  state = {
    target: this.props.store.$group.selected ? "group" : "individual"
  };
  handleGroupChange = e => {
    this.setState({ target: e.target.value });
  };
  render() {
    const { $survey, $user, $group, $router } = this.props.store;

    return (
      $survey.selected && (
        <React.Fragment>
          <h3>Deel de enquête</h3>
          <div className="mt2 mb2">
            Deel onderstaande link om de enquête in te vullen:
          </div>

          <C.Radio
            value={"individual"}
            label={"Individueel invullen"}
            className="mr2"
            type="radio"
            onChange={this.handleGroupChange}
            checked={this.state.target === "individual"}
            name="target"
          />
          <C.Radio
            label={"Als groep/school invullen"}
            value={"group"}
            type="radio"
            onChange={this.handleGroupChange}
            checked={this.state.target === "group"}
            name="target"
          />

          {this.state.target === "group" && (
            <C.Select
              options={$group.filteredList.map(group => ({
                value: group._key,
                label: group.name
              }))}
              visible={this.state.target === "group"}
              placeholder={"Geen groep/school"}
              disabled={!$user.currentUser.isCreator}
              label={"Als groep/school invullen:"}
              onChange={e =>
                $group.change(
                  "selected",
                  clone($group.list.get(e.target.value))
                )
              }
              name={"group"}
              value={$group.selected ? $group.selected._key : undefined}
              defaultValue={
                $user.currentUser.groups || $group.selected
                  ? $group.selected._key
                  : undefined
              }
              className="mt2"
            />
          )}

          <div className="flex items-end bg-primary p2 mt2">
            {this.state.target === "group" ? (
              <C.Input
                label=""
                readOnly
                className="width-full"
                value={`http://${window.location.host}/survey/view/${
                  $survey.selected._key
                }${
                  $group.selected
                    ? "/?group=" + $group.selected._key
                    : $user.currentUser.groups
                      ? "/?group=" + $user.currentUser.groups
                      : ""
                }`}
                target="_blank"
              />
            ) : (
              <C.ButtonNew
                label="Ga naar de enquête"
                class="btn bg-primary"
                onClick={() =>
                  $router.push("/survey/view/" + $survey.selected._key)
                }
              />
            )}
          </div>
        </React.Fragment>
      )
    );
  }
};

export default observer(SurveyShare);

// generateURLParams = () => {
//   const prefills = this.state.prefills;tqz
//   Object.keys(prefills).forEach(key => prefills[key]);
//   return Object.keys(prefills)[0]
//     ? `?${Object.keys(prefills)[0]}=${prefills[Object.keys(prefills)[0]]}`
//     : '';
// };

// const questionList = $survey.selected
// ? [...$survey.selected.questions.template, ...$survey.selected.questions.survey]
// : [];

// <h4>Vooraf ingevulde velden</h4>
// <C.Form className="flex" name="prefills">
//   <C.Select
//     options={questionList
//       .filter(question => question.type === 'hidden')
//       .map(question => ({
//         value: question._key,
//         label: $locale.t(question._key)
//       }))}
//     label={'Veld'}
//     name={'key1'}
//     className="mt1 col-6"
//     readOnly
//   />
//   <C.Select
//     options={values($school.list)}
//     keys={['_key', 'naam']}
//     className="mt1 col-6"
//     label="Waarde"
//     name={'value1'}
//     value={
//       (document.forms.prefills &&
//         this.state.prefills[document.forms.prefills.key1.value]) ||
//       ''
//     }
//     onChange={e => {
//       let prefills = Object.create(this.state.prefills);
//       prefills[document.forms.prefills.key1.value] = e.target.value;
//       this.setState({ prefills });
//     }}
//   />
// </C.Form>
