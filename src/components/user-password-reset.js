import React, { Component } from "react";
import * as C from "vonnegut.components";
import { observer, inject } from "mobx-react";

const UserPasswordReset = class extends Component {
  state = {
    email: "",
    step: "password_reset"
  };

  render() {
    const { $user, $locale } = this.props.store;
    return (
      <C.ModalNew
        open={$user.passwordResetEnabled}
        width="650px"
        height="350px"
        classNameContent="no-margin bg-gray-lightest p3"
        noCloseButton
        handleClose={() => $user.change("passwordResetEnabled", false)}
        actions={[
          <C.ButtonNew
            key={"1"}
            className={"bg-transparent"}
            onClick={() => $user.change("passwordResetEnabled", false)}
            label={$locale.t("cancel")}
          />,
          <C.ButtonNew
            key={"2"}
            className="btn bg-primary"
            onClick={async e => {
              e.preventDefault();
              const email = this.state.email;
              await $user.passwordReset(email);
            }}
            label={$locale.t("send")}
          />
        ]}
      >
        <h2 className="color-primary">
          {$locale.t("user_password_reset_title")}
        </h2>
        <div
          className="mt2 mb2"
          dangerouslySetInnerHTML={{
            __html: $locale.t("user_password_reset_body")
          }}
        />
        <form name="user-password-reset" style={{ height: "auto" }}>
          <C.Input
            required
            type="email"
            value={this.state.email}
            name="email"
            placeholder="E-mail"
            onChange={e => this.setState({ email: e.target.value })}
          />
        </form>
      </C.ModalNew>
    );
  }
};

export default inject("store")(observer(UserPasswordReset));
