import React, { Component } from 'react';
import * as C from 'vonnegut.components';
import { observer, inject } from 'mobx-react';
import { clone } from 'mobx-state-tree';
import * as Icon from 'react-feather';
import DialogConfirm from './_common/dialog-confirm';
import { values } from 'mobx';

const UserList = class extends Component {
  state = {
    confirmUser: false
  };
  cols = [
    {
      label: 'actief',
      type: 'select',
      condition: row => !row['disabled'],
      width: '10%'
    },
    { label: 'email', key: 'email', width: '45%' },
    { label: 'groep', key: 'group', width: '15%' },
    { label: 'gebruikersrol', key: 'roles', width: '45%' },
    {
      type: 'action',
      icon: <Icon.Edit2 />,
      className: 'bg-transparent',
      onClick: (e, user) => {
        this.props.store.$user.change('selected', clone(user));
        this.props.store.$router.push('/user/edit/' + user.uid);
      },
      width: '45px'
    },
    {
      type: 'action',
      icon: <Icon.Trash2 />,
      className: 'bg-transparent',
      onClick: (e, user) => this.setState({ confirmUser: user }),
      width: '45px'
    }
  ];
  render() {
    const { store } = this.props;
    const { confirmUser } = this.state;
    const { list } = store.$user;
    console.log('render user list', list);
    return (
      <div>
        <C.List cols={this.cols} className="zebra">
          {values(list).map(user => (
            <C.ListRow key={user.uid} row={user} {...user} />
          ))}
        </C.List>
        <DialogConfirm
          text={'Bent u zeker dat u deze gebruiker wil verwijderen?'}
          open={confirmUser}
          onSubmit={() => list.get(confirmUser.uid).delete()}
          onClose={() => this.setState({ confirmUser: false })}
        />
      </div>
    );
  }
};
export default inject('store')(observer(UserList));
