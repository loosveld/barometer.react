import React from 'react';
import * as C from 'vonnegut.components';

const DialogConfirm = ({ title, text, open, onSubmit, onClose, labelSubmit, labelClose }) => {
  return (
    <C.ModalNew
      open={open}
      title={title}
      classNameContent="no-margin p3 bg-gray-lightest"
      actions={[
        <C.ButtonNew
          onClick={onClose}
          label={labelClose || 'Annuleren'}
          className={'bg-transparent'}
        />,
        <C.ButtonNew onClick={onSubmit} label={labelSubmit || 'OK'} className={'bg-orange'} />
      ]}
    >
      {text && text}
    </C.ModalNew>
  );
};

export default DialogConfirm;
