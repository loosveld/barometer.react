import React from 'react';
import * as C from 'vonnegut.components';
import { inject, observer } from 'mobx-react';

const LanguageSwitcher = ({ store }) =>
  store.$survey.selected &&
  store.$survey.selected.languages.split(',').length > 1 && (
    <div className="language-switcher">
      <C.Select
        options={store.$survey.selected.languages
          .split(',')
          .map(key => ({ key, label: store.$locale.getLabelForKey(key) }))}
        name="activeLocale"
        keys={['key', 'label']}
        defaultValue={store.$locale.activeLanguage}
        onChange={store.$locale.change2}
      />
    </div>
  );

export default inject('store')(observer(LanguageSwitcher));
