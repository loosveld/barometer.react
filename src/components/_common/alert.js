import React from 'react';
import * as C from 'vonnegut.components';
import * as Icon from 'react-feather';
import { inject, observer } from 'mobx-react';
import { entries } from 'mobx';

const Alert = ({ store }) =>
  store.$alert.list.size > 0 && (
    <div className="alert-container">
      {entries(store.$alert.list).map(a => (
        <div className="alert" key={a[0]}>
          {(!a[1].label || a[1].loader === 'circle') && (
            <C.LoaderSpinner className="ml2" type={a[1].loader || 'circle'} />
          )}
          {a[1].label && <span className="label">{a[1].label}</span>}
          {a[1].label && (
            <C.ButtonNew className="ml1 bg-transparent" onClick={e => store.$alert.delete(a[0])}>
              <Icon.X />
            </C.ButtonNew>
          )}
        </div>
      ))}
    </div>
  );

export default inject('store')(observer(Alert));
