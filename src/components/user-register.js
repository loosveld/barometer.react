import React, { Component } from "react";
import * as C from "vonnegut.components";
import { observer, inject } from "mobx-react";
import * as Icon from "react-feather";

const UserRegister = class extends Component {
  state = {
    group_enabled: !this.props.store.$config.register_as_individual
      ? true
      : false,
    group: "",
    email: "",
    password: "",
    password_confirm: ""
  };
  isPasswordValid = () => {
    return (
      this.state.password &&
      this.state.password_confirm &&
      this.state.password === this.state.password_confirm
    );
  };
  renderSwitch = param => {
    const { $config, $locale } = this.props.store;

    switch (param) {
      case "register":
        return (
          <React.Fragment>
            <h2 className="color-primary">
              {$locale.t("register_register_title")}
            </h2>
            <div
              className="mt2 mb2"
              dangerouslySetInnerHTML={{
                __html: $locale.t("register_register_body")
              }}
            />
            <form name="user-register" className="width-full">
              <C.Input
                required
                type="email"
                value={this.state.email}
                name="email"
                placeholder="E-mail"
                className="mb2"
                onChange={e => this.setState({ email: e.target.value })}
              />
              <C.Input
                required
                type="password"
                value={this.state.password}
                name="password"
                placeholder="Paswoord"
                className="mb2"
                onChange={e => this.setState({ password: e.target.value })}
              />
              <C.Input
                required
                type="password"
                value={this.state.password_confirm}
                name="password_confirm"
                placeholder="Bevestig Paswoord"
                className="mb2"
                onChange={e =>
                  this.setState({ password_confirm: e.target.value })
                }
                icon={this.isPasswordValid() && <Icon.Check />}
              />
              {$config.register_as_group && (
                <React.Fragment>
                  <div className="field">
                    <label />
                    <div className="control">
                      <C.CheckboxNew
                        label={$locale.t("register_register_group_enable")}
                        className="mb2 width-full"
                        disabled={!$config.register_as_individual}
                        checked={this.state.group_enabled}
                        onChange={e =>
                          this.setState({
                            group_enabled: !this.state.group_enabled
                          })
                        }
                      />
                    </div>
                  </div>
                  {this.state.group_enabled && (
                    <C.Input
                      label="School/Groep"
                      required
                      placeholder="School/group"
                      value={this.state.group}
                      name="group"
                      className="mb2"
                      onChange={e => this.setState({ group: e.target.value })}
                    />
                  )}
                </React.Fragment>
              )}
            </form>
          </React.Fragment>
        );
      case "register_success":
        return (
          <React.Fragment>
            <h2 className="color-primary">
              {$locale.t("register_success_title")}
            </h2>
            <div className="mt2">{$locale.t("register_success_text")}</div>
          </React.Fragment>
        );
      default:
        return <div>default</div>;
    }
  };
  render() {
    const {
      $user,
      $config,
      $survey,
      $alert,
      $locale,
      $validation,
      $router
    } = this.props.store;
    return (
      <C.ModalNew
        open={$user.registerEnabled}
        width="650px"
        height="600px"
        classNameContent="no-margin bg-gray-lightest p3"
        noCloseButton
        handleClose={() => $user.change("registerEnabled", false)}
        actions={[
          <C.ButtonNew
            key={"1"}
            className={
              $user.registerStep === "register_success"
                ? "bg-primary"
                : "bg-transparent"
            }
            onClick={() => {
              $user.change("registerEnabled", false);
              if ($user.registerStep === "register_success") {
                if ($user.currentUser.isMentor) {
                  $router.push("/survey");
                } else {
                  if ($survey.selected) {
                    $router.push("/survey/view/" + $survey.selected._key);
                  } else {
                    $router.push($config.home);
                  }
                }
              }
            }}
            label={
              $user.registerStep === "register_success"
                ? "OK"
                : $locale.t("cancel")
            }
          />,
          <C.ButtonNew
            key={"2"}
            className="btn bg-primary"
            disabled={!this.state.email || !this.isPasswordValid()}
            visible={$user.registerStep !== "register_success"}
            onClick={async e => {
              e.preventDefault();
              const email = this.state.email;
              const password = this.state.password;
              const group = this.state.group;
              const validations = $validation.validateForm("user-register");
              if (
                group &&
                group.value === "" &&
                (!$config.register_as_individual || $config.register_as_group)
              ) {
                $alert.delete("loading");
                $alert.add("error", { label: "No group" });
              } else {
                const response = await $user.createUserWithEmailAndPassword(
                  email,
                  password,
                  group
                );
                if (response) {
                  $user.change("registerStep", "register_success");
                }
              }
            }}
            label={$locale.t("register_register_btn_label")}
          />
        ]}
      >
        {this.renderSwitch($user.registerStep)}
      </C.ModalNew>
    );
  }
};

export default inject("store")(observer(UserRegister));
