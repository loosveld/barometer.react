import React, { Component } from 'react';
import * as C from 'vonnegut.components';
import { observer, inject } from 'mobx-react';
import { values } from 'mobx';

const UserEdit = class extends Component {
  componentWillUnmount = () => {
    this.props.store.$validation.clearList();
  };
  render() {
    const { $user, $router, $validation, $group } = this.props.store;

    return (
      $user.selected && (
        <C.Form name="user-edit">
          {$user.currentUser.isAdmin && <div className="mb2">{$user.selected.uid}</div>}
          <C.Input
            onChange={$user.selected.changeAndValidate}
            label="E-mail"
            name="email"
            value={$user.selected.email}
            className="mb2 row"
            required
            type="email"
            disabled={!$user.currentUser.isAdmin}
            validation={{ label: $validation.getValidationKey('email', 'user-edit') }}
          />
          {$router.mode === 'new' && (
            <C.Input
              label="Wachtwoord"
              name="password"
              value={$user.selected.password}
              autocomplete="off"
              className="mb2 row"
              minLength="6"
              pattern=".{6,}"
              required
              type="password"
              validation={{ label: $validation.getValidationKey('password', 'user-edit') }}
              onChange={$user.selected.changeAndValidate}
            />
          )}
          {$user.currentUser.isCreator && (
            <C.Input
              label="Rollen "
              name="roles"
              className="row mb1"
              value={$user.selected.roles}
              description="auth, mentor, creator"
              onChange={$user.selected.changeAndValidate}
            />
          )}
          <C.Select
            options={values($group.list).map(g => ({ value: g._key, label: g.name }))}
            label="Groep"
            name="groups"
            className="row mb2"
            defaultValue={values($group.list).find(g => g._key === $user.currentUser.groups)}
            disabled={!$user.currentUser.isAdmin}
            value={$user.selected.groups || ''}
            onChange={$user.selected.changeAndValidate}
            description="school, organisatie,..."
          />
          <C.Checkbox
            checked={!$user.selected.disabled}
            name="disabled"
            disabled={!$user.currentUser.isAdmin}
            onChange={(e, checked) => $user.selected.change('disabled', !checked)}
            label="Actief"
          />
        </C.Form>
      )
    );
  }
};

export default inject('store')(observer(UserEdit));
