import React from "react";
import { clone } from "mobx-state-tree";
import { observer, inject } from "mobx-react";
import * as C from "vonnegut.components";
import * as dateFns from "date-fns";

const SurveyResultsFilter = ({ store }) => (
  <div className="mt2 flex mxn2">
    {store.$user.currentUser.isMentor && (
      <C.Select
        options={store.$group.filteredList.map(group => ({
          value: group._key,
          label:
            group.name +
            " (" +
            store.$survey.selected.respondents.filter(
              r => r.groupKey === group._key
            ).length +
            " respondenten)"
        }))}
        placeholder={
          "Alle groepen/scholen (" +
          store.$survey.selected.respondents.length +
          " respondenten)"
        }
        label={"Groep/school"}
        onChange={e => {
          if (e.target.value) {
            store.$group.change(
              "selected",
              clone(store.$group.list.get(e.target.value))
            );
          } else {
            store.$group.destroy(store.$group.selected);
          }

          store.$survey.selected &&
            store.$survey.load(store.$survey.selected._key);
        }}
        name={"group"}
        disabled={
          store.$user.currentUser.isMentor &&
          !store.$user.currentUser.isCreator &&
          store.$user.currentUser.groups.split(",").length === 1
        }
        defaultValue={
          (store.$group.selected && store.$group.selected._key) || ""
        }
        className="mx2 col-6"
      />
    )}
    {store.$survey.selected.interval_period !== "none" && (
      <C.Select
        options={store.$survey.selected.availableIntervalPeriods.map(
          period => ({
            value: new Date(period.startDate),
            label: `${dateFns.format(
              period.startDate,
              "DD/MM/YY"
            )} > ${dateFns.format(period.endDate, "DD/MM/YY")}`
          })
        )}
        className={"mx2 col-6"}
        placeholder={false}
        onChange={e => {
          store.$survey.setActiveIntervalPeriod(e.target.value);
          store.$survey.selected &&
            store.$survey.load(store.$survey.selected._key);
        }}
        defaultValue={String(store.$survey.activeIntervalPeriod) || ""}
        label="Beschikbare Periodes"
      />
    )}
  </div>
);

export default inject("store")(observer(SurveyResultsFilter));
