import React from 'react';
import * as C from 'vonnegut.components';
import { observer, inject } from 'mobx-react';
import * as Icon from 'react-feather';
import * as dateFns from 'date-fns';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { values } from 'mobx';

const SurveySettings = class extends React.Component {
  render() {
    const { $survey, $validation, $locale, $user, $alert, $config } = this.props.store;

    return (
      $survey.selected && (
        <C.Form name="new-survey" onSubmit={$survey.selected.fsSave}>
          {this.props.mode === 'settings' && <h3>Instellingen</h3>}
          <div className="mt3">
            {$user.currentUser.isAdmin && (
              <div className="field row mb2">
                <label>Created By</label>
                <div>{$survey.selected.createdBy}</div>
              </div>
            )}
            <C.InputNew
              value={$survey.selected.name}
              label="Naam"
              name="name"
              placeholder={`Naam van ${
                $survey.selected.isTemplate ? 'het sjabloon' : 'de enquête'
              }`}
              className="row mb2"
              required
              disabled={!$user.currentUser.isCreator}
              onChange={$survey.selected.changeAndValidate}
              invalidText={$validation.getValidationKey('name', 'new-survey')}
            />
            {$config.templates &&
              $user.currentUser.isCreator &&
              this.props.mode === 'new' && (
                <div className="field row">
                  <label />
                  <div className="control">
                    <C.CheckboxNew
                      label="Deze enquete als sjabloon toevoegen?"
                      name="isTemplate"
                      className="mb2 width-full"
                      checked={$survey.selected.isTemplate}
                      disabled={$survey.selected._key !== 'new'}
                      onChange={$survey.selected.changeAndValidate}
                    />
                  </div>
                </div>
              )}
            {$config.templates &&
              this.props.mode === 'new' &&
              !$survey.selected.isTemplate && (
                <C.Select
                  name="template"
                  label="Sjabloon"
                  options={$survey.templates}
                  keys={['_key', 'name']}
                  className="row mb2 top"
                  placeholder="Kies een sjabloon ..."
                  onChange={$survey.selected.changeAndValidate}
                  validation={{ label: $validation.getValidationKey('template', 'new-survey') }}
                />
              )}
            {this.props.mode === 'settings' && (
              <div className="field row mb2">
                <label>Status</label>
                <C.CheckboxNew
                  label="Actief"
                  disabled={!$user.currentUser.isCreator}
                  name="enabled"
                  className="row"
                  checked={$survey.selected.enabled}
                  onChange={$survey.selected.changeAndValidate}
                />
              </div>
            )}
            {this.props.mode === 'settings' && (
              <div className="field row mb2 mt2">
                <label>Talen</label>
                <div className="control flex flex-wrap">
                  {$config.languages.map(language => (
                    <C.CheckboxNew
                      label={language.label}
                      key={language.key}
                      name="languages"
                      className="row mb2"
                      disabled={!$user.currentUser.isCreator || $config.languages.length < 2}
                      value={language.key}
                      checked={$survey.selected.languages.split(',').indexOf(language.key) > -1}
                      onChange={$survey.selected.changeLanguages}
                    />
                  ))}
                </div>
              </div>
            )}
            {$survey.selected &&
              !$survey.selected.enabled && (
                <C.Input
                  value={$survey.selected.disabledMessage}
                  label="Boodschap inactief"
                  disabled={!$user.currentUser.isCreator}
                  className="row mb2 mt2"
                  onChange={e => $survey.selected.change('disabledMessage', e.target.value)}
                />
              )}
            {this.props.mode === 'settings' && (
              <C.Select
                name="interval_period"
                label="Interval Periode"
                disabled={!$user.currentUser.isCreator}
                options={[
                  { value: 'none', label: 'Geen' },
                  { value: 'day', label: '1 x per dag' },
                  { value: 'week', label: '1 x per week' },
                  { value: 'month', label: '1 x per maand' },
                  { value: 'trimester', label: '1 x per trimester' },
                  { value: 'year', label: '1 x per jaar' }
                ]}
                defaultValue={$survey.selected.interval_period}
                className="row mb2"
                placeholder="Kies een interval ..."
                onChange={$survey.selected.changeAndValidate}
                validation={{
                  label: $validation.getValidationKey('interval_period', 'new-survey')
                }}
              />
            )}
            {this.props.mode === 'settings' &&
              $survey.selected.interval_period &&
              $survey.selected.interval_period !== 'none' && (
                <div className="field mb2 row">
                  <label>Interval startdatum</label>
                  <div className="control">
                    <DayPickerInput
                      name="interval_start"
                      value={$survey.selected.interval_start || new Date()}
                      className="width-full"
                      formatDate={date => dateFns.format(date, 'DD/MM/YYYY')}
                      hideOnDayClick={true}
                      onDayChange={date => {
                        $survey.selected.change('interval_start', date);
                      }}
                      inputProps={{ type: 'text', disabled: !$user.currentUser.isCreator }}
                    />
                  </div>
                </div>
              )}
            {this.props.mode === 'settings' && (
              <div className="field mb2">
                <label>Intro</label>
                <div className="control">
                  {$survey.selected.languages.split(',').map(
                    (language, i) =>
                      language && (
                        <div
                          key={language}
                          className={`flex mxn1 ${i + 1 !== $survey.selected.languages.length &&
                            'mb1'}`}
                        >
                          <C.Input value={language.substring(0, 2)} disabled className="mx1" />
                          <C.Textarea
                            wysiwyg={false}
                            className="width-full mx1"
                            disabled={!$user.currentUser.isCreator}
                            value={$locale.t($survey.selected._key + 'introText', language)}
                            rows={20}
                            onChange={e =>
                              $locale.setSelected(
                                $survey.selected._key + 'introText',
                                language,
                                e.target.value
                              )
                            }
                          />
                        </div>
                      )
                  )}
                </div>
              </div>
            )}
            {this.props.mode === 'settings' && (
              <div className="field mb2">
                <label>Feedback</label>
                <div className="control">
                  {$survey.selected.languages.split(',').map(
                    (language, i) =>
                      language && (
                        <div
                          key={language}
                          className={`flex mxn1 ${i + 1 !== $survey.selected.languages.length &&
                            'mb1'}`}
                        >
                          <C.Input value={language.substring(0, 2)} disabled className="mx1" />
                          <C.Textarea
                            wysiwyg={false}
                            className="width-full mx1"
                            disabled={!$user.currentUser.isCreator}
                            value={$locale.t($survey.selected._key + '_feedback', language)}
                            rows={20}
                            onChange={e =>
                              $locale.setSelected(
                                $survey.selected._key + '_feedback',
                                language,
                                e.target.value
                              )
                            }
                          />
                        </div>
                      )
                  )}
                </div>
              </div>
            )}
          </div>
          {$user.currentUser.isCreator &&
            $survey.selected._key !== 'new' && (
              <C.ButtonNew
                className={['bg-primary', 'mt3']}
                type="submit"
                disabled={$alert.list.size > 0}
              >
                <Icon.Save className="mr2" />
                Bewaren
              </C.ButtonNew>
            )}
        </C.Form>
      )
    );
  }
};

export default inject('store')(observer(SurveySettings));
