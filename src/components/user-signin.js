import React, { Component } from 'react';
import * as C from 'vonnegut.components';
import { observer, inject } from 'mobx-react';

const UserSignIn = class extends Component {
  render() {
    const { $user, $locale } = this.props.store;
    return (
      <form
        name="user-signin"
        className="p3"
        onSubmit={e => {
          e.preventDefault();
          $user.signIn(e.target.email.value, e.target.password.value);
        }}
      >
        <div className="mb3">
          <h2 class="color-primary">Aanmelden</h2>
        </div>
        <C.Input
          label="Email"
          name="email"
          type="email"
          required
          className="mb2"
        />
        <C.Input
          label="Wachtwoord"
          type="password"
          name="password"
          className="mb3"
          required
        />
        <div className="flex justify-between">
          <div>
            <C.ButtonNew className="bg-primary" type="submit">
              Inloggen
            </C.ButtonNew>
            {$user.status !== "done" && $user.status}
          </div>
          <div className="flex items-center color-red">
            <C.ButtonNew
              className="bg-transparent"
              onClick={e => {
                if (!$user.currentUser.uid) {
                  $user.change("passwordResetEnabled", true);
                }
              }}
            >
              {$locale.t("user_password_reset_title")}
            </C.ButtonNew>
            <C.ButtonNew
              className="bg-transparent"
              onClick={e => {
                if (!$user.currentUser.uid) {
                  $user.change("registerEnabled", true);
                }
              }}
            >
              Registreren
            </C.ButtonNew>
          </div>
        </div>
      </form>
    );
  }
};

export default inject('store')(observer(UserSignIn));
