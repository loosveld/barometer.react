import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import QuestionContainer from "./question";
import * as dateFns from "date-fns";
import SurveyViewWelcome from "./survey-view-welcome";

const SurveyView = class extends Component {
  showForm = () => {
    let s = this.props.store.$survey.selected;
    return (
      (s.enabled &&
        (s.answersForCurrentUID.length === 0 ||
          (s.answersForCurrentUID.length > 0 &&
            s.interval_period === "none"))) ||
      (s.interval_period !== "none" &&
        dateFns.compareAsc(Date.now(), s.expireDate) > -1)
    );
  };

  getMostRecentAnswerDate = answers => {
    return answers.map(a => a.createdOn).sort(dateFns.compareDesc);
  };

  render() {
    const { $survey } = this.props.store;

    return (
      $survey.selected && (
        <section className="border-radius-large">
          {$survey.selected.answersForCurrentUID.length > 0 && (
            <div className="bg-orange p2 border-radius color-white mb2">
              Deze survey is laatst ingevuld op{" "}
              {dateFns.format(
                this.getMostRecentAnswerDate(
                  $survey.selected.answersForCurrentUID
                )[0],
                "DD/MM/YYYY"
              )}
              .{" "}
              {$survey.selected.interval_period !== "none" && (
                <span>
                  Opnieuw invullen mogelijk vanaf{" "}
                  {dateFns.format($survey.selected.expireDate, "DD/MM/YYYY")}.
                </span>
              )}
              <br />
              <a href={"/survey/results/" + $survey.selected._key}>
                Bekijk resultaten
              </a>
            </div>
          )}
          {this.showForm() ? (
            <React.Fragment>
              {!$survey.selected._disableWelcome && (
                <SurveyViewWelcome survey={$survey.selected} />
              )}
              {$survey.selected._disableWelcome && (
                <form name={"survey-" + $survey.selected._key}>
                  <QuestionContainer
                    {...this.props}
                    survey={$survey.selected}
                    mode="view"
                  />
                </form>
              )}
            </React.Fragment>
          ) : (
            <div>{$survey.selected.disabledMessage}</div>
          )}
        </section>
      )
    );
  }
};

export default inject("store")(observer(SurveyView));
