import React from 'react';
import * as C from 'vonnegut.components';
import { observer, inject } from 'mobx-react';
import ReactMarkdown from 'react-markdown';
import * as Icon from 'react-feather';
import { clone } from 'mobx-state-tree';

const SurveyViewWelcome = ({ store, survey }) =>
  survey && (
    <div className="flex justify-center items-center flex-column height-full p2">
      <h1 className="center">
        <Icon.CheckSquare size={36} color="#444" />
      </h1>
      <h1 className="center color-primary">{survey.name}</h1>
      {store.$locale.t(survey._key + 'introText') && (
        <ReactMarkdown
          className="mt2 lg-col-10 p3 bg-white border-radius-large"
          source={store.$locale.t(survey._key + 'introText')}
        />
      )}
      {store.$group.selected && (
        <div className="mt2">
          <span>U vult deze enquête in voor&nbsp;</span>
          <b>{store.$group.selected.name}</b>
        </div>
      )}
      <C.ButtonNew
        className="bg-primary mt3 medium mb2"
        onClick={e => {
          const { $router, $survey, $user } = store;
          if (!$user.currentUser.uid) {
            !$survey.selected && $survey.setSelected(clone(survey));
            $user.change('registerEnabled', true);
          } else {
            $router.page !== 'survey' &&
              $router.mode !== 'view' &&
              $router.push(`/survey/view/${survey._key}`);
            $survey.selected.change('_disableWelcome', true);
          }
        }}
        icon={<Icon.Play />}
        label="START"
      />
    </div>
  );

export default inject('store')(observer(SurveyViewWelcome));
