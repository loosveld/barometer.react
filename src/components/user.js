import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import * as C from "vonnegut.components";

import UserEdit from "./user-edit";
import UserList from "./user-list";
import UserRegister from "./user-register";
import UserSignIn from "./user-signin";
import UserPasswordReset from "./user-password-reset";

const UserContainer = class extends Component {
  render() {
    const { $user, $router } = this.props.store;

    return $user.currentUser.isMentor ? (
      $user.currentUser.isAdmin ? (
        <React.Fragment>
          <header className="flex justify-between">
            <h2>Gebruikers</h2>
          </header>
          <div className="p3">
            {(!$router.mode || $router.mode === "list") && <UserList />}
            {($router.mode === "new" || $router.mode === "edit") && (
              <C.ModalNew
                open={$user.selected}
                classNameContent="no-margin p3 bg-gray-lightest"
                width="850px"
                height="600px"
                title={`${
                  $router.mode === "new"
                    ? "Gebruiker toevoegen"
                    : "Gebruiker aanpassen"
                }`}
                noCloseButton
                handleClose={() => {
                  $user.change("selected", null);
                  $router.push("/user");
                }}
                actions={[
                  <C.ButtonNew
                    onClick={() => {
                      $user.change("selected", null);
                      $router.push("/user");
                    }}
                    label={"Annuleren"}
                    className={"bg-transparent"}
                  />,
                  <C.ButtonNew
                    onClick={() => $user.selected.save()}
                    label={$router.mode === "new" ? "Voeg Toe" : "Bewaar"}
                    className={"bg-primary"}
                  />
                ]}
              >
                <UserEdit />
              </C.ModalNew>
            )}
          </div>
        </React.Fragment>
      ) : (
        $router.mode === "edit" && (
          <React.Fragment>
            <header className="flex justify-between">
              <h2>{$user.currentUser.email}</h2>
            </header>
            <div class="p3">
              <UserEdit />
            </div>
          </React.Fragment>
        )
      )
    ) : (
      <React.Fragment>
        {$router.mode === "signin" && (
          <React.Fragment>
            <UserSignIn />
            <UserPasswordReset />
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
};

export default inject("store")(observer(UserContainer));
