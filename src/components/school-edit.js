import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import * as C from 'vonnegut.components';

const SchoolEdit = class extends Component {
  render() {
    const { $school } = this.props.store;

    return (
      <C.Form name="school-edit">
        {Object.keys($school.selected.$treenode.type.properties).map(key => (
          <div>
            <C.Input label={key} value={$school.selected[key]} disabled />
          </div>
        ))}
      </C.Form>
    );
  }
  en;
};

export default inject('store')(observer(SchoolEdit));
