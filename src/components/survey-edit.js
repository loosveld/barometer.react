import React from 'react';
import { observer, inject } from 'mobx-react';
import QuestionContainer from './question';
import LanguageSwitcher from './_common/language-switcher';

const SurveyEdit = ({ store }) => (
  <React.Fragment>
    <div className="flex justify-between">
      <h3>Vragen</h3>
      <LanguageSwitcher />
    </div>
    <div className="mt3">{store.$survey.selected && <QuestionContainer />}</div>
  </React.Fragment>
);

export default inject('store')(observer(SurveyEdit));
