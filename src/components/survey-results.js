import React, { Component } from 'react';
import { clone } from 'mobx-state-tree';
import { observer, inject } from 'mobx-react';
import * as C from 'vonnegut.components';
import * as Icon from 'react-feather';
import * as dateFns from 'date-fns';
import SurveyResultsFilter from './survey-results-filter';
import { values } from 'mobx';
import { appConfig } from '../config';

import * as Chart from 'recharts';

const SurveyResults = class extends Component {
  dataRadar = [];
  dataBar = [];
  dataRadial = {};
  fieldTypeScale = appConfig.field_types.find(field => field.value === 'scale');

  generateDataRadial = (question, user) => {
    let data = { individual: [], group: [] };

    const avi = question.getAnswerValuesForIndividual(user.uid);
    const avg = question.getAnswerValuesForGroup(user.groups);

    const field = appConfig.field_types.find(field => field.value === 'scale');

    const individual = field.options.map(option => ({
      color: option.color,
      name: option.label,
      value: Math.round(
        (avi.filter(v => String(v) === String(option.value)).length / avi.length) * 100
      )
    }));

    const group = field.options.map(option => ({
      color: option.color,
      name: option.label,
      value: Math.round(
        (avg.filter(v => String(v) === String(option.value)).length / avg.length) * 100
      )
    }));

    data.individual = individual || [];
    data.group = group || [];
    console.log('radial', data);
    return data;
  };

  generateDataBarPerFieldset = (question, user) => {
    const field = appConfig.field_types.find(field => field.value === 'scale');

    return question.children.map(question => {
      if (question.type === 'fieldset') {
        const avi = question.getAnswerValuesForIndividual(user.uid);
        const avg = question.getAnswerValuesForGroup(user.groups);
        return {
          name: this.props.store.$locale.t(question._key),
          vo: -Math.round((avg.filter(v => v === '10').length / avg.length) * 100),
          o: -Math.round((avg.filter(v => v === '20').length / avg.length) * 100),
          e: Math.round((avg.filter(v => v === '30').length / avg.length) * 100),
          ve: Math.round((avg.filter(v => v === '40').length / avg.length) * 100)
        };
      }
    });
  };

  generateDataBarForQuestion = (question, user) => {
    let data = [];
    const fieldTypeScale = appConfig.field_types.find(field => field.value === 'scale');

    const avi = question.getAnswerValuesForIndividual(user.uid);
    const avg = question.getAnswerValuesForGroup(user.groups);

    let object = { name: 'individual' };
    fieldTypeScale.options.filter(option => {
      object[option.key] = Math.round(
        (avi.filter(v => v === String(option.value)).length / avi.length) * 100
      );
    });
    data.push(object);

    object = { name: 'group' };
    fieldTypeScale.options.filter(option => {
      object[option.key] = Math.round(
        (avg.filter(v => v === String(option.value)).length / avg.length) * 100
      );
    });
    data.push(object);
    return data;
  };

  render() {
    const { $survey, $user, $locale, $question, $answer, $group } = this.props.store;
    // if (
    //   $survey.status === 'done' &&
    //   $question.status === 'done' &&
    //   $locale.status === 'done' &&
    //   $answer.status === 'done'
    // ) {
    //   $survey.selected.questionTree.forEach(question => {
    //     let parent = $question.list.get(question._key);
    //     this.dataRadar.push({
    //       subject: $locale.t(parent._key),
    //       individual: parent.getAverageAnswerValue({ by: 'uid', value: $user.currentUser.uid }),
    //       group: parent.getAverageAnswerValue({ by: 'groupKey', value: $user.currentUser.group }),
    //       fullMark: 100
    //     });

    //     let avg = parent.getAnswerValuesForGroup($user.currentUser.group);
    //     let avi = parent.getAnswerValuesForIndividual($user.currentUser.uid);

    //     this.dataBar.push({
    //       name: $locale.t(parent._key),
    //       vo: -Math.round((avg.filter(v => v === '10').length / avg.length) * 100),
    //       o: -Math.round((avg.filter(v => v === '20').length / avg.length) * 100),
    //       e: Math.round((avg.filter(v => v === '30').length / avg.length) * 100),
    //       ve: Math.round((avg.filter(v => v === '40').length / avg.length) * 100)
    //     });
    //   });

    //   console.log('this.dataRadar', this.dataRadar);
    //   console.log('this.dataRadial', this.dataRadial);
    //   console.log('this.dataBar', this.dataBar);
    // }

    return (
      $survey.selected && (
        <div>
          <div className="flex justify-between">
            <h2>Resultaten</h2>
            <Icon.Printer />
          </div>
          <SurveyResultsFilter />

          <div className="mt3">
            {($user.currentUser.isAuth && !$survey.selected.answersForCurrentUID.length) ||
              ($user.currentUser.isMentor &&
                $survey.selected.answers.length === 0 && (
                  <div className="bg-orange p2 border-radius color-white mb2">
                    Geen resultaten gevonden.
                  </div>
                ))}
            {/*level 1*/}
            {$survey.selected.questionTree.map(level1 => {
              let dataBarPerFieldset = [];
              let dataBarForQuestion = [];
              let dataRadial = {};
              level1 = $question.list.get(level1._key);

              console.log('Level 1 question: ', $locale.t(level1._key), level1.children.length);
              if (level1.children.length > 0) {
                if (level1.children.filter(q => q.type.indexOf('scale') > -1).length) {
                  dataRadial = this.generateDataRadial(level1, $user.currentUser);
                } else {
                  dataBarPerFieldset = this.generateDataBarPerFieldset(level1, $user.currentUser);
                }
              } else {
                dataBarForQuestion = this.generateDataBarForQuestion(level1, $user.currentUser);
              }

              console.log('dataRadial', dataRadial);
              console.log('dataBarPerFieldset', dataBarPerFieldset);
              console.log('dataBarForQuestion', dataBarForQuestion);

              return (
                <div className="bg-white border-radius border border-gray p2 flex justify-center flex-column mb2 center items-center">
                  <h3>{$locale.t(level1._key)}</h3>

                  {dataBarPerFieldset.length > 0 && (
                    <Chart.BarChart
                      layout="vertical"
                      width={700}
                      height={400}
                      data={dataBarPerFieldset}
                      stackOffset="sign"
                      margin={{ left: 100 }}
                    >
                      <Chart.CartesianGrid strokeDasharray="3 3" />
                      <Chart.XAxis type="number" />
                      <Chart.YAxis dataKey="name" type="category" padding={{ left: 100 }} />
                      <Chart.Legend />
                      <Chart.ReferenceLine x={0} stroke="#000" />
                      <Chart.Bar dataKey="vo" stackId="a" barSize="20" fill="darkred" />
                      <Chart.Bar dataKey="o" stackId="a" barSize="20" fill="red" />
                      <Chart.Bar dataKey="e" stackId="a" barSize="20" fill="green" />
                      <Chart.Bar dataKey="ve" stackId="a" barSize="20" fill="darkgreen" />
                    </Chart.BarChart>
                  )}
                  {dataBarForQuestion.length > 0 && (
                    <Chart.BarChart
                      layout="vertical"
                      width={150}
                      height={50}
                      data={dataBarForQuestion}
                      margin={{ left: 100 }}
                    >
                      <Chart.Legend />
                      <Chart.Bar dataKey="vo" stackId="a" barSize="20" fill="darkred" />
                      <Chart.Bar dataKey="o" stackId="a" barSize="20" fill="red" />
                      <Chart.Bar dataKey="e" stackId="a" barSize="20" fill="green" />
                      <Chart.Bar dataKey="ve" stackId="a" barSize="20" fill="darkgreen" />
                    </Chart.BarChart>
                  )}
                  {Object.keys(dataRadial).length && (
                    <div>
                      <Chart.PieChart width={300} height={300}>
                        <Chart.Pie
                          data={dataRadial.individual}
                          dataKey="value"
                          nameKey="name"
                          cx="50%"
                          cy="50%"
                          outerRadius={50}
                        >
                          {dataRadial.individual.map((entry, index) => (
                            <Chart.Cell fill={entry.color} />
                          ))}
                        </Chart.Pie>
                        <Chart.Pie
                          data={dataRadial.group}
                          dataKey="value"
                          nameKey="name"
                          cx="50%"
                          cy="50%"
                          innerRadius={60}
                          outerRadius={80}
                          fill="#82ca9d"
                          label="----"
                        >
                          {dataRadial.individual.map((entry, index) => (
                            <Chart.Cell fill={entry.color} />
                          ))}
                        </Chart.Pie>
                        <Chart.Tooltip />
                      </Chart.PieChart>
                      {/*level 2*/}
                      {level1.children.map(level2 => {
                        let dataBarPerFieldset = [];
                        let dataBarForQuestion = [];
                        let dataRadial = {};
                        level2 = $question.list.get(level2._key);

                        console.log(
                          'Level 2 question: ',
                          $locale.t(level2._key),
                          level2.children.length
                        );

                        if (level2.children.length > 0) {
                          if (level2.children.filter(q => q.type.indexOf('scale') > -1).length) {
                            dataRadial = this.generateDataRadial(level2, $user.currentUser);
                          } else {
                            dataBarPerFieldset = this.generateDataBarPerFieldset(
                              level2,
                              $user.currentUser
                            );
                          }
                        } else {
                          dataBarForQuestion = this.generateDataBarForQuestion(
                            level2,
                            $user.currentUser
                          );
                        }

                        console.log('dataRadial', dataRadial);
                        console.log('dataBarForQuestion', dataBarForQuestion);

                        return (
                          <div className="flex justify-between">
                            <div className="left-align">{$locale.t(level2._key)}</div>
                            {Object.keys(dataRadial).length && (
                              <Chart.PieChart width={300} height={300}>
                                <Chart.Pie
                                  data={dataRadial.individual}
                                  dataKey="value"
                                  nameKey="name"
                                  cx="50%"
                                  cy="50%"
                                  outerRadius={50}
                                >
                                  {dataRadial.individual.map((entry, index) => (
                                    <Chart.Cell fill={entry.color} />
                                  ))}
                                </Chart.Pie>
                                <Chart.Pie
                                  data={dataRadial.group}
                                  dataKey="value"
                                  nameKey="name"
                                  cx="50%"
                                  cy="50%"
                                  innerRadius={60}
                                  outerRadius={80}
                                  fill="#82ca9d"
                                  label="----"
                                >
                                  {dataRadial.individual.map((entry, index) => (
                                    <Chart.Cell fill={entry.color} />
                                  ))}
                                </Chart.Pie>
                                <Chart.Tooltip />
                              </Chart.PieChart>
                            )}
                            {dataBarForQuestion.length > 0 && (
                              <div className="flex">
                                <Chart.BarChart
                                  width={50}
                                  height={100}
                                  data={[dataBarForQuestion[0]]}
                                >
                                  {this.fieldTypeScale.options.map(option => (
                                    <Chart.Bar
                                      dataKey={option.key}
                                      stackId="a"
                                      fill={option.color}
                                    />
                                  ))}
                                </Chart.BarChart>
                                <Chart.BarChart
                                  width={50}
                                  height={100}
                                  data={[dataBarForQuestion[1]]}
                                >
                                  {this.fieldTypeScale.options.map(option => (
                                    <Chart.Bar
                                      dataKey={option.key}
                                      stackId="a"
                                      fill={option.color}
                                    />
                                  ))}
                                </Chart.BarChart>
                              </div>
                            )}
                          </div>
                        );
                      })}
                    </div>
                  )}
                  <div />
                </div>
              );
            })}
          </div>
        </div>
      )
    );
  }
};

//$survey.selected && <QuestionContainer {...this.props} mode="results" />

export default inject('store')(observer(SurveyResults));

//{/*General*/}
// {this.dataRadar.length > 0 && (
//   <div className="border border-gray border-radius flex justify-center items-center flex-column">
//     <Chart.RadarChart outerRadius={90} width={710} height={300} data={this.dataRadar}>
//       <Chart.PolarGrid />
//       <Chart.PolarAngleAxis dataKey="subject" />
//       <Chart.Radar
//         name="Individueel"
//         dataKey="individual"
//         stroke="#007db3"
//         fill="#007db3"
//         fillOpacity={0.6}
//       />
//       <Chart.Radar
//         name="Group"
//         dataKey="group"
//         stroke="#1b905c"
//         fill="#1b905c"
//         fillOpacity={0.6}
//       />
//       <Chart.Tooltip />
//       <Chart.Legend />
//     </Chart.RadarChart>
//   </div>
// )}
