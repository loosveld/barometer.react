import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import * as C from 'vonnegut.components';
import * as Icon from 'react-feather';
import SchoolEdit from './school-edit.js';
import { values } from 'mobx';

const SchoolContainer = class extends Component {
  cols = [
    { label: 'Key', key: '_key', width: '15%' },
    { label: 'Naam', key: 'naam', width: '100%' },
    {
      type: 'action',
      icon: <Icon.Edit2 />,
      className: 'bg-transparent',
      onClick: (e, row) => this.props.store.$router.push('/school/edit/' + row._key)
    }
  ];
  componentDidMount = async () => {
    await this.props.store.$school.load();
  };
  render() {
    const { $school, $router, $validation } = this.props.store;
    return (
      <React.Fragment>
        <header className="flex justify-between">
          <h2>Scholen</h2>
        </header>
        <div className="p3">
          {$router.mode === 'edit' && (
            <C.ModalNew
              open={$school.selected}
              width="950px"
              height="700px"
              classNameContent="no-margin bg-gray-lightest"
              title={$school.selected._key === 'new' ? 'School toevoegen' : $school.selected.naam}
              noCloseButton
              handleClose={() => {
                $school.detach($school.selected);
              }}
              actions={[
                <C.ButtonNew
                  key={'1'}
                  className="bg-transparent"
                  onClick={() => {
                    this.props.store.$router.push('/school');
                    $school.detach($school.selected);
                  }}
                  label={'Annuleren'}
                />,
                <C.ButtonNew
                  key={'2'}
                  onClick={() => {
                    const validations = $validation.validateForm('school-edit');
                    if (validations.size === 0) {
                      $school.selected.fsSave();
                      $school.detach($school.selected);
                    }
                  }}
                  label={$school.selected._key === 'new' ? 'Voeg toe' : 'Bewaren'}
                />
              ]}
            >
              <SchoolEdit {...this.props} />
            </C.ModalNew>
          )}

          <div>
            <C.Search onChange={target => false} name="filter-search" />
            <div className="mt2 mb2">
              <b>{$school.list.size > 0 && $school.list.size}</b>
              &nbsp;scholen gevonden
            </div>
            <C.List cols={this.cols} className="zebra">
              {values($school.list).map(school => (
                <C.ListRow
                  key={school._key}
                  row={school}
                  onRowClick={(e, row) => this.props.store.$router.push('/school/edit/' + row._key)}
                />
              ))}
            </C.List>
          </div>
        </div>
      </React.Fragment>
    );
  }
};

export default inject('store')(observer(SchoolContainer));
