import { types } from 'mobx-state-tree';
import { getURLParameterByName } from '../helpers';

window.addEventListener('hashchange', () => console.log('test'));

export const Router = types
  .model({
    pathname: window.location.pathname,
    previousPathname: types.optional(types.string, '')
  })
  .actions(self => ({
    push(path) {
      console.log('router push', path);
      self.previousPathname = self.path;
      self.pathname = path;
      window.history.pushState({}, 'title', self.pathname);
    },
    onAction() {
      console.log('router action', self.pathname);
    }
  }))
  .views(self => ({
    getURLParameterByName(name) {
      return getURLParameterByName(name);
    },
    get params() {
      return self.pathname.split('/');
    },
    get page() {
      return self.params[1];
    },
    get mode() {
      return self.params[2];
    },
    get id() {
      return self.params[3];
    },
    get key() {
      return self.params[3];
    },
    get key2() {
      return self.params[4];
    },
    get previousParams() {
      return self.previousPathname.split('/');
    },
    get previousPage() {
      return self.previousParams[1];
    },
    matchPath(pathname, { path, exact }) {
      return false;
    }
  }));
