import { types, applySnapshot } from 'mobx-state-tree';
//import { FirebaseModel } from 'mobx-state-tree-firebase';
import { db } from '../firebase.js';
import { actions } from './_common.js';

const schoolsRef = db.collection('schools');

//import data from '../data/basis.json';
// data.forEach(school => {
//   schoolsRef
//     .doc('OND' + school.schoolnummer)
//     .set(school);
// });

export const School = types.compose(
  types.model({
    _key: types.maybeNull(types.string),
    schoolnummer: types.maybeNull(types.string),
    intern_vplnummer: types.maybeNull(types.string),
    vestigingsnummer: types.maybeNull(types.string),
    net: types.maybeNull(types.string),
    naam: types.maybeNull(types.string),
    hoofdzetel: types.maybeNull(types.string),
    straat: types.maybeNull(types.string),
    huisnummer: types.maybeNull(types.string),
    postcode: types.maybeNull(types.string),
    gemeente: types.maybeNull(types.string),
    //'crab-code': types.maybeNull(types.string),
    //'crab-huisnr': types.maybeNull(types.string),
    //'crab-busnr': types.maybeNull(types.string),
    telefoon: types.maybeNull(types.string),
    fax: '',
    'e-mail': types.maybeNull(types.string),
    url: types.maybeNull(types.string),
    'familienaam beheerder': types.maybeNull(types.string),
    'voornaam beheerder': types.maybeNull(types.string)
    //Lx: types.maybeNull(types.string),
    //Ly: types.maybeNull(types.string)
  }),
  actions
);

export const SchoolStore = types.compose(
  types
    .model({
      list: types.optional(types.map(School), {}),
      status: types.optional(types.string, 'pending'),
      selected: types.maybeNull(School)
    })
    .actions(self => ({
      load() {
        schoolsRef.onSnapshot(snapshot => {
          let schools = {};
          snapshot.docs.forEach(doc => {
            let school = {};
            //convert all values to string
            Object.keys(doc.data()).forEach(key => (school[key] = String(doc.data()[key])));
            school._key = doc.id;
            schools[doc.id] = school;
          });
          applySnapshot(self.list, schools);
          self.setProp('status', 'done');
        });
      }
    })),
  actions
);
