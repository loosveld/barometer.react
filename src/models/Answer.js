import { types, getRoot } from "mobx-state-tree";
import { db } from "../firebase.js";
import { removeInternalProps } from "../helpers/index.js";
import { actions } from "./_common.js";
import { values } from "mobx";
import FirestoreDate from "./FirestoreDate";

export const fieldOptions = types.model({
  value: types.maybeNull(types.string)
});

const refSurveys = db.collection("surveys");

export const Answer = types.compose(
  types
    .model({
      _key: types.maybeNull(types.string),
      _path: types.maybeNull(types.string),
      questionKey: types.maybeNull(types.string),
      surveyKey: types.maybeNull(types.string),
      createdBy: types.maybeNull(types.string),
      groupKey: types.maybeNull(types.string),
      value: types.maybeNull(types.string),
      createdOn: types.maybeNull(FirestoreDate),
      ip: types.maybeNull(types.string)
    })
    .actions(self => ({}))
    .views(self => ({})),
  actions
);

export const AnswerStore = types.compose(
  types
    .model({
      list: types.optional(types.map(Answer), {}),
      tempList: types.optional(types.map(Answer), {}),
      status: types.optional(types.string, "pending"),
      selectedAnswer: types.maybeNull(Answer)
    })
    .actions(self => ({
      afterCreate() {},
      setToTempList: function(e, question) {
        const validation = getRoot(self).$validation.validate(e.target);
        if (
          Object.keys(validation).length === 0 &&
          getRoot(self).$router.mode === "view"
        ) {
          const surveyKey = getRoot(self).$survey.selected._key;
          const questionKey = question._key;
          const _key = surveyKey + "_" + questionKey;
          getRoot(self).$survey.selected.disableQuestionFromLogic({
            value: e.target.value,
            questionKey
          });
          self.tempList.set(_key, {
            _key,
            surveyKey,
            questionKey,
            createdBy: getRoot(self).$user.currentUser.uid,
            groupKey: getRoot(self).$user.currentUser.groups,
            value: e.target.value
          });
          localStorage.setItem(
            "barometer-answers",
            JSON.stringify(self.tempList.toJSON())
          );
        }
      },
      saveTempList() {
        let batch = db.batch();
        values(self.tempList).forEach(answer => {
          let answerRef = refSurveys
            .doc(answer.surveyKey)
            .collection("answers")
            .doc();
          answer = removeInternalProps(answer);
          answer.createdOn = new Date();
          batch.set(answerRef, answer);
        });

        return batch.commit();
      }
    }))
    .views(self => ({
      // get answersForCurrentUID() {
      //   Object.values(self.list).filter(answer=> answer.uid === getRoot(self).currentUser.uid && answer. )
      // }
    })),
  actions
);
