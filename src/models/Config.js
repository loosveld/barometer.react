import { types } from 'mobx-state-tree';
import { appConfig } from '../config';

const ConfigStore = types
  .model({
    id: types.optional(types.string, 'barometer'),
    appName: types.optional(types.string, 'BAROMETER'),
    templates: types.optional(types.boolean, false),
    home: types.optional(types.string, '/start'),
    languages: types.frozen([{ key: 'nl_BE', label: 'Nederlands' }]),
    register_as_individual: types.optional(types.boolean, true),
    register_as_group: types.optional(types.boolean, false),
    register_register_title: types.optional(types.string, 'Registratie'),
    register_register_body: types.optional(
      types.string,
      'Nadat we jouw emailadres hebben ontvangen mailen we je de nodige gegevens om aan de slag te gaan.'
    ),
    register_register_group_enable: types.optional(
      types.string,
      'DISCO als school/groep invullen?'
    ),
    color1: types.optional(types.string, '#348a66'),
    color2: types.optional(types.string, '#53bc76'),
    field_types: types.frozen([]),
    results: types.frozen([])
  })
  .actions(self => ({
    afterCreate() {
      self.load();
    },
    load() {
      Object.keys(appConfig).forEach(key => {
        self.change(key, appConfig[key]);
      });
    },
    change(key, value) {
      self[key] = value;
    }
  }));

export default ConfigStore;
