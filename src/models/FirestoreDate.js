import { types } from 'mobx-state-tree';

const FirestoreDate = types.custom({
  name: 'FirestoreDate',
  fromSnapshot(value) {
    return value ? value.toDate() : new Date();
  },
  toSnapshot(value) {
    return value;
  },
  isTargetType(value) {
    return value instanceof Date;
  },
  getValidationMessage(value) {
    if (value instanceof Object) return '';
    else return 'geen timestamp';
  }
});

export default FirestoreDate;
