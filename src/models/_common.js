import { types, getRoot, detach, destroy, getChildType } from 'mobx-state-tree';

export const actions = types.model({}).actions(self => ({
  changeAndValidate: function(event) {
    const field = event.target;
    try {
      const validation = getRoot(self).$validation;
      const type = getChildType(self, field.name).name;
      if (type.indexOf('string') > -1) {
        self[field.name] = field.value || '';
      } else if (type.indexOf('boolean') > -1) {
        self[field.name] = field.checked;
      } else if (type.indexOf('number') > -1) {
        self[field.name] = Number(field.value);
      } else {
        self[field.name] = field.value && field.value !== 'on' ? field.value : field.checked;
      }
      validation && validation.validate(field);
    } catch (e) {
      console.error(e, 'Probably no validation model in root.');
    }
  },
  change: function(property, value) {
    self[property] = value;
  },
  change2: function(e) {
    console.log('change2', e.target.name, e.target.value);
    self[e.target.name] = e.target.value;
  },
  setProp(property, value) {
    self.change(property, value);
  },
  destroy(item) {
    item && destroy(item); //zet bv. selected op null
  },
  detach(item) {
    item && detach(item);
  },
  setToList(key, value) {
    key && value && self.list.set(key, { _key: key, ...value });
  },
  popFromList(value) {
    self.list.remove(value);
  },
  deleteFromList(key, list = self.list) {
    list.delete(key);
  },
  clearList() {
    self.list.clear();
  }
}));

// export const actions = types.model({}).actions(self => ({
//   changeAndValidate: function(event) {
//     const field = event.target;
//     try {
//       const validation = getRoot(self).$validation;
//       self[field.name] = field.value || field.checked;
//       validation && validation.validate(field);
//     } catch (e) {
//       console.error(e, 'Probably no validation model in root.');
//     }
//   },
//   change: function(property, value) {
//     self[property] = value;
//   },
//   setProp(property, value) {
//     self.change(property, value);
//   },
//   destroy(item) {
//     item && destroy(item); //zet bv. selected op null
//   },
//   detach(item) {
//     item && detach(item);
//   },
//   setToList(key, value) {
//     key && value && self.list.set(key, value);
//   },
//   pushToList(value) {
//     value && self.list.push(value);
//   },
//   //search & remove item from array
//   popFromList(value) {
//     self.list.remove(value);
//   },
//   deleteFromList(key) {
//     self.list.delete(key);
//   },
//   clearList() {
//     self.list.clear();
//   }
// }));
