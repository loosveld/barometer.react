import { types } from 'mobx-state-tree';

export const Model = types.model({
  label: types.maybeNull(types.string),
  time: types.optional(types.number, 0),
  loader: types.maybeNull(types.string)
});

export const store = types
  .model({
    list: types.optional(types.map(Model), {})
  })
  .actions(self => ({
    add(key, alert) {
      self.list.set(key, alert ? alert : {});
      alert &&
        alert.time &&
        setTimeout(() => {
          self.delete(key);
        }, alert.time);
    },
    delete(key) {
      self.list.delete(key);
    }
  }));
