import { types, flow, getRoot, applySnapshot } from "mobx-state-tree";
import { db } from "../firebase.js";
import { actions } from "./_common.js";

//const ref = db.collection('locale');
const refNew = db.collection("localeNew");

const translation = types.model("translation", {
  language: types.optional(types.string, ""),
  translation: types.optional(types.string, "")
});

export const Locale = types.compose(
  types
    .model("Locale", {
      _key: types.identifier,
      translations: types.optional(types.map(translation), {}),
      parentKey: types.optional(types.string, "")
    })
    .actions(self => ({
      fsSave: flow(function*() {
        let ref = refNew.doc(self.parentKey);
        let locale = {};
        locale = Object.assign(
          {},
          {
            parentKey: self.parentKey,
            translations: self.translations.toJSON()
          }
        );
        yield ref.set(locale);
        getRoot(self).$locale.setToList(self.parentKey, locale);
      }),
      fsDelete: flow(function*() {
        getRoot(self).$alert.add("loading", {
          loader: "circle"
        });
        yield refNew.doc(self._key).delete();
        getRoot(self).$alert.delete("loading");
      })
    }))
    .views(self => ({})),
  actions
);

export const LocaleStore = types.compose(
  types
    .model({
      list: types.optional(types.map(Locale), {}),
      status: types.optional(types.string, "pending"),
      selectedList: types.optional(types.map(Locale), {}),
      activeLocale: "nl_BE"
    })
    .actions(self => ({
      load: flow(function*(parentKeys) {
        const snapshot = yield refNew.get();
        let list = {};
        snapshot.docs.forEach(doc => {
          list[doc.id] = doc.data();
          list[doc.id]._key = doc.id;
        });
        getRoot(self).$config.locale &&
          getRoot(self).$config.locale.forEach(item => {
            list[item.parentKey] = {};
            list[item.parentKey]._key = item.parentKey;
            list[item.parentKey] = { ...list[item.parentKey], ...item };
          });
        applySnapshot(self.list, list);
        self.setProp("status", "done");
        return snapshot;
      }),
      setSelected(key, language, translation) {
        const locale = self.getLocaleByParentKey(key);
        const localeSelected = self.getLocaleByParentKey(
          key,
          self.selectedList
        );
        let translations = {};
        if (locale) {
          translations = locale.translations.toJSON();
        }
        if (localeSelected) {
          translations = Object.assign(
            {},
            translations,
            localeSelected.translations.toJSON()
          );
        }
        if (language) {
          translations = Object.assign({}, translations, {
            [language]: { translation, language: language }
          });
        }
        key &&
          self.selectedList.set(key, {
            _key: key,
            parentKey: key,
            translations
          });
      },
      clearSelectedList() {
        self.selectedList.clear();
      }
    }))
    .views(self => ({
      getLabelForKey(key) {
        let lang = getRoot(self).$config.languages.find(
          lang => lang.key === key
        );
        return lang ? lang.label : false;
      },
      getLocaleByParentKey(parentKey, list = self.list) {
        return list.has(parentKey) ? list.get(parentKey) : false;
      },
      getTranslation(parentKey, language = self.activeLocale) {
        const locale = self.getLocaleByParentKey(parentKey);
        const localeSelected = self.getLocaleByParentKey(
          parentKey,
          self.selectedList
        );
        let translations = false;
        let translation = "";
        if (locale) {
          translations = locale.translations;
        }
        if (localeSelected) {
          translations = localeSelected.translations;
        }
        if (translations) {
          translation = translations.has(language)
            ? translations.get(language).translation
            : "";
        }
        return translation;
      },
      t(parentKey, language = self.activeLocale) {
        return self.getTranslation(parentKey, language);
      }
    })),
  actions
);
