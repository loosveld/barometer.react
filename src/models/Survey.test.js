import { getSnapshot, onPatch, onSnapshot } from 'mobx-state-tree';
import { SurveyStore, Survey, TipList } from './Survey';
import { reaction } from 'mobx';

it('surveyitem: create instance', () => {
  const item = Survey.create({ name: 'test survey' });
  expect(item.name).toBe('test survey');
});

it('surveylist: create & load', () => createAndLoad(TipList));

it('surveylist: create & load', () => createAndLoad(TipList));

const createAndLoad = model => {
  const list = model.create();

  const patches = [];

  onSnapshot(list, patch => {
    patches.push(patch);
  });

  console.log('SurveyStore length:', Object.keys(list.items).length);

  expect(patches).toMatchSnapshot();
};
