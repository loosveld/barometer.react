import { types } from "mobx-state-tree";

const validityList = {
  badInput: false, // value does not conform to the pattern
  rangeOverflow: false, // value of a number field is higher than the max attribute
  rangeUnderflow: false, // value of a number field is lower than the min attribute
  stepMismatch: false, // value of a number field does not conform to the stepattribute
  tooLong: false, // the user has edited a too-long value in a field with maxlength
  tooShort: false, // the user has edited a too-short value in a field with minlength
  typeMismatch: false, // value of a email or URL field is not an email address or URL
  valueMissing: false, // required field without a value
  customError: false,
  patternMismatch: false
};

const Validation = types.model("Validation", {
  fieldName: types.maybeNull(types.string),
  formName: types.maybeNull(types.string),
  key: types.maybeNull(types.string)
});

export const ValidationStore = types
  .model("ValidationStore", { list: types.optional(types.map(Validation), {}) })
  .actions(self => ({
    setToList(validation) {
      let key = validation.formName + "__" + validation.fieldName;
      self.list.has(key) && self.list.get(key).clear();
      self.list.set(key, validation);
    },
    deleteFromList(validationKey) {
      self.list.delete(validationKey);
    },
    clearList() {
      self.list.clear();
    },
    getValidityKey(field) {
      if (field.validity && !field.validity.valid) {
        for (var key in validityList) {
          if (field.validity[key]) return key;
        }
      } else {
        return false;
      }
    },
    validate(field) {
      let validation = {};
      try {
        if (field.form) {
          self.deleteFromList(field.form.name + "__" + field.name);
          for (var key in validityList) {
            if (
              field.validity &&
              !field.validity.valid &&
              field.validity[key]
            ) {
              let key = this.getValidityKey(field);
              key &&
                self.setToList(
                  Validation.create({
                    fieldName: field.name,
                    formName: field.form.name,
                    key: `validation_${key}`
                  })
                );
            }
          }
        }
      } catch (e) {
        console.error(e, "Probably no <form> defined");
      }
      return validation;
    },
    validateForm(formName) {
      const form = document.forms[formName];
      if (form) {
        Object.keys(form.elements).forEach(key => {
          self.validate(form.elements[key]);
        });
      }
      console.log(self.list);
      return self.list;
    }
  }))
  .views(self => ({
    getValidationKey(fieldName, formName) {
      return (
        self.list.has(formName + "__" + fieldName) &&
        self.list.get(formName + "__" + fieldName).key
      );
    }
  }));
