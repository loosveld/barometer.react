import { types, flow, getRoot, clone, applySnapshot } from "mobx-state-tree";
import { db } from "../firebase.js";
import { actions } from "./_common.js";
import { values } from "mobx";
import * as H from "../helpers";

const refSurveys = db.collection("surveys");

export const Condition = types
  .model({
    question: types.maybeNull(types.string),
    condition: types.optional(types.string, "is"),
    answer: types.maybeNull(types.string)
  })
  .actions(self => ({
    change(e) {
      self[e.target.name] = e.target.value;
    }
  }));

export const Logic = types
  .model({
    conditions: types.optional(types.map(Condition), {}),
    action: types.maybeNull(types.string)
    //questionKey: types.maybeNull(types.string)
  })
  .actions(self => ({
    change(e) {
      self[e.target.name] = e.target.value;
    },
    setToConditionList(key, value) {
      self.conditions.set(key, {
        ...value
      });
    },
    deleteFromConditionList(key) {
      console.log("delete from condi", key);
      self.conditions.delete(key);
    }
  }));

export const fieldOption = types
  .model({
    value: types.maybeNull(types.string),
    label: types.maybeNull(types.string)
  })
  .actions(self => ({
    change: function(value, type) {
      if (type === "label") {
        self.value = H.slugify(value);
        self.label = value;
      } else {
        self.value = H.slugify(value);
      }
    }
  }));

export const Question = types.compose(
  types
    .model("Question", {
      _key: types.identifier,
      _path: types.maybeNull(types.string),
      name: types.optional(types.string, ""), //deprecated
      parentKey: types.maybeNull(types.string),
      type: types.optional(types.string, "text"),
      typeOptions: types.optional(types.array(fieldOption), []),
      createdOn: types.optional(types.number, Date.now()),
      logic: types.maybeNull(Logic),
      createdBy: types.maybeNull(types.string),
      required: types.optional(types.boolean, false),
      surveyKey: types.maybeNull(types.string),
      order: types.optional(types.number, 0),
      description: types.optional(types.string, ""),
      feedback: types.optional(types.string, ""),
      enabled: types.optional(types.boolean, true)
    })
    .actions(self => ({
      fsSave: flow(function*() {
        const { $alert, $question, $locale, $survey } = getRoot(self);
        let keyNew = self._key === "new";
        $alert.add("loading", {
          loader: "circle"
        });
        const ref = refSurveys.doc(self.surveyKey).collection("questions");
        let questionDoc = !keyNew ? ref.doc(self._key) : ref.doc();
        //prepare question
        let question = Object.assign({}, self.toJSON());
        delete question._key;
        delete question._path;
        //save or update question / also update state since we dont use onsnapshot
        yield !keyNew
          ? questionDoc.update(question)
          : questionDoc.set(question);
        $question.setToList(questionDoc.id, question);
        //update and save locale
        values($locale.selectedList).forEach(locale => {
          if (locale.parentKey === "new") {
            locale.change("parentKey", questionDoc.id);
          }
          locale.fsSave();
          //getRoot(self).$locale.
        });
        //save survey (updateby en on)
        $survey.selected.fsSave();
        $alert.delete("loading");
        self._key === "new" &&
          $alert.add("added", {
            label: "Vraag toegevoegd",
            time: 5000
          });
        return questionDoc;
      }),
      fsDelete: flow(function*() {
        let root = getRoot(self);
        root.$alert.add("loading", {
          loader: "circle"
        });
        let questionKey = self._key;
        let surveyKey = self.surveyKey;
        if (self.type === "fieldset") {
          const items = yield refSurveys
            .doc(surveyKey)
            .collection("questions")
            .where("parentKey", "==", questionKey)
            .get();
          items.docs.forEach(doc => doc.ref.delete());
        }
        const locale = root.$locale.getLocaleByParentKey(questionKey);
        locale.fsDelete();
        yield refSurveys
          .doc(surveyKey)
          .collection("questions")
          .doc(questionKey)
          .delete();
        root.$alert.delete("loading");
      }),

      initLogic(e) {
        if (e.target.checked) {
          self.logic = {
            action: ""
          };
          self.logic.setToConditionList(Date.now(), {
            question: getRoot(self).$question.selected._key,
            condition: "is",
            answer: ""
          });
        } else {
          self.logic = null;
        }
      },
      addToTypeOptions: function(value) {
        self.typeOptions.push({
          value,
          label: value
        });
      }
    }))
    .views(self => ({
      get answerTemp() {
        const surveyKey = getRoot(self).$survey.selected._key;
        const questionKey = self._key;
        const _key = surveyKey + "_" + questionKey;
        return getRoot(self).$answer.tempList.get(_key);
      },
      get answers() {
        const { $survey } = getRoot(self);
        return $survey.selected._answers.filter(
          a => a.questionKey === self._key
        );
      },
      getAnswerValuesForGroup(groupKey) {
        let answers;
        if (!self.children.length) {
          //no fieldset
          answers = getRoot(self).$survey.selected._answers.filter(
            a => a.questionKey === self._key
          );
        } else {
          //fieldset
          answers = getRoot(self).$survey.selected._answers.filter(a =>
            self.children.find(child => a.questionKey === child._key)
          );
        }
        return answers
          .filter(a => a.groupKey === groupKey || !groupKey)
          .map(a => a.value);
      },
      getAnswerValuesForIndividual(uid) {
        let answers;
        if (!self.children.length) {
          //no fieldset
          answers = getRoot(self).$survey.selected._answers.filter(
            a => a.questionKey === self._key
          );
        } else {
          //fieldset
          answers = getRoot(self).$survey.selected._answers.filter(a =>
            self.children.find(child => a.questionKey === child._key)
          );
        }
        return answers.filter(a => a.createdBy === uid).map(a => a.value);
      },
      getAverageAnswerValue(filter) {
        let values =
          (filter && filter.by === "groupKey") || !filter
            ? self.getAnswerValuesForGroup(filter && filter.value)
            : self.getAnswerValuesForIndividual(filter.value);

        let sumValues = values.reduce((a, b) => Number(a) + Number(b), 0);
        return values.length ? sumValues / values.length : values.length;
      },
      get translations() {
        return values(getRoot(self).$locale.list).map(
          translation => translation.parentKey === self._key && translation
        );
      },
      get isEditable() {
        return self.surveyKey === getRoot(self).$survey.selected._key;
      },
      get children() {
        return values(getRoot(self).$question.list).filter(
          q => q.parentKey === self._key
        );
      },
      get childrenRecursive() {
        let allChildren = [];
        let level1 = values(getRoot(self).$question.list).filter(
          q => q.parentKey === self._key
        );

        allChildren.push(level1);
        level1.forEach(fieldset => {
          let level2 = values(getRoot(self).$question.list).filter(
            q => q.parentKey === fieldset._key
          );
          allChildren.push(level2);
          level2.forEach(fieldset => {
            let level3 = values(getRoot(self).$question.list).filter(
              q => q.parentKey === fieldset._key
            );
            allChildren.push(level3);
          });
        });

        return [].concat
          .apply([], allChildren)
          .filter(child => child.type !== "fieldset");
      }
    })),
  actions
);

export const QuestionStore = types.compose(
  types
    .model({
      list: types.optional(types.map(Question), {}),
      status: types.optional(types.string, "pending"),
      selected: types.maybeNull(Question),
      activeQuestion: 0
    })
    .actions(self => ({
      load(ref) {
        return ref.get().then(snapshot => {
          let list = {};
          snapshot.forEach(doc => {
            list[doc.id] = doc.data();
            list[doc.id]._key = doc.id;
          });
          applySnapshot(self.list, list);
          self.setProp("status", "done");
        });
      },
      setSelected(question) {
        self.change(
          "selected",
          question._key === "new" ? question : clone(question)
        );
      },
      prevActiveQuestion(e) {
        e.preventDefault();
        self.activeQuestion--;
      },
      nextActiveQuestion(e) {
        e.preventDefault();
        self.activeQuestion++;
      }
    })),
  actions
);
