import { types, flow, getRoot } from "mobx-state-tree";
import { db } from "../firebase.js";
import { actions } from "./_common.js";
import { values } from "mobx";
import { clone } from "mobx-state-tree";
import { removeInternalProps } from "../helpers";
import FirestoreDate from "./FirestoreDate";

const groupsRef = db.collection("groups");

export const Group = types.compose(
  types
    .model({
      _key: types.maybeNull(types.string),
      name: types.maybeNull(types.string),
      createdBy: types.maybeNull(types.string),
      createdOn: FirestoreDate
    })
    .actions(self => ({
      fsSave: flow(function*() {
        let keyNew = self._key === "new" || !self._key;
        let doc = !keyNew ? groupsRef.doc(self._key) : groupsRef.doc();
        //prepare group
        let group = removeInternalProps(self.toJSON());
        //save or update question / also update state since we dont use onsnapshot
        yield !keyNew ? doc.update(group) : doc.set(group);
        getRoot(self).$group.setToList(doc.id, group);
        return doc;
      })
    })),
  actions
);

export const GroupStore = types.compose(
  types
    .model({
      list: types.optional(types.map(Group), {}),
      status: types.optional(types.string, "pending"),
      selected: types.maybeNull(Group)
    })
    .views(self => ({
      get filteredList() {
        const { currentUser } = getRoot(self).$user;
        const list = currentUser.isCreator
          ? values(self.list)
          : values(self.list).filter(
              group => currentUser.groups.indexOf(group._key) > -1
            );
        return list;
      }
    }))
    .actions(self => ({
      load: flow(function*() {
        try {
          const snapshot = yield groupsRef.get();
          snapshot.docs.forEach(doc => {
            self.setToList(doc.id, doc.data());
          });
          getRoot(self).$user.currentUser.groups &&
            self.change(
              "selected",
              clone(self.list.get(getRoot(self).$user.currentUser.groups))
            );
          self.setProp("status", "done");
        } catch (error) {
          console.log("error", error);
        }
      })
    })),
  actions
);
