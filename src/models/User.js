import {
  types,
  getParent,
  getRoot,
  destroy,
  flow,
  clone
} from "mobx-state-tree";
import { db, auth } from "../firebase.js";
import { actions } from "./_common.js";
import { entries } from "mobx";
import FirestoreDate from "./FirestoreDate";

export const roles = [
  {
    value: "admin",
    label: "Admin"
  },
  {
    value: "creator",
    label: "Creator"
  },
  {
    value: "mentor",
    label: "Begeleider"
  },
  {
    value: "auth",
    label: "Authenticated"
  },
  {
    value: "anonymous",
    label: "Anonymous"
  }
];

const ref = db.collection("users");

export const User = types.compose(
  types
    .model("user", {
      uid: types.optional(types.string, ""),
      email: types.maybeNull(types.string, ""),
      emailVerified: types.optional(types.boolean, false),
      password: types.optional(types.string, ""),
      disabled: types.optional(types.boolean, false),
      metadata: types.maybe(
        types.model("metadata", {
          lastSignInTime: types.string,
          creationTime: types.string
        })
      ),
      ip: types.maybe(types.string),
      roles: types.optional(types.string, ""), //meerde roles moeten mogelijk zijn, aanpassen
      groups: types.maybeNull(types.string), //aanpassen naar Group model
      language: types.optional(types.string, "nl_BE"),
      createdOn: types.maybeNull(FirestoreDate),
      allowedSurveys: types.maybeNull(types.string, ""),
      isAnonymous: types.maybeNull(types.boolean)
    })
    .actions(self => ({
      save: flow(function* create() {
        let root = getRoot(self);
        root.$alert.add("loading", {
          loader: "circle"
        });
        let validations = root.$validation.validateForm("user-edit");
        if (validations.size === 0) {
          const response = yield _fetch(
            `https://us-central1-barometer-disco.cloudfunctions.net/${
              self.uid === "new" ? "createUser" : "updateUser"
            }`,
            JSON.stringify(self.toJSON())
          );
          const user = {
            ...self,
            ...(yield response.json())
          };

          //root.$group.selected.fsSave();
          if (user) {
            //store metadata
            yield ref.doc(user.uid).set({
              roles: self.roles,
              groups: self.groups.toJSON(),
              language: self.language,
              email: self.email,
              createdOn: new Date()
            });

            //getParent(self).delete(self); werkt niet!
            getParent(self).setToList(user.uid, prepareUser(user));
            root.$alert.delete("loading");
          }
        }
      }),
      delete: flow(function*() {
        getRoot(self).$alert.add("loading", {
          loader: "circle"
        });
        const response = yield _fetch(
          "https://us-central1-barometer-disco.cloudfunctions.net/deleteUser",
          JSON.stringify(self.toJSON())
        );
        const user = yield response.json();
        if (user) {
          getRoot(self).$alert.delete("loading");
          destroy(getParent(self).list.get(self.uid));
        }
      })
    }))
    .views(self => ({
      get isAdmin() {
        return self.roles.indexOf("admin") > -1;
      },
      get isCreator() {
        return self.roles.indexOf("creator") > -1;
      },
      get isMentor() {
        return self.roles.indexOf("mentor") > -1;
      },
      get isAuth() {
        return self.roles.indexOf("auth") > -1;
      },
      // get isTemp() {
      //   return (
      //     self.roles.indexOf("auth") > -1 && self.roles.indexOf("temp") > -1
      //   );
      // },
      // get isAnonymous() {
      //   return self.isAnonymous; //!self.roles || !self.uid;
      // },
      get isPrivate() {
        return self.isAdmin || self.isCreator;
      }
    })),
  actions
);

export const UserStore = types.compose(
  types
    .model("UserStore", {
      list: types.optional(types.map(User), {}),
      status: types.optional(types.string, ""),
      selected: types.maybeNull(User),
      currentUser: types.optional(User, {}),
      storageKey: "KEY_FOR_LOCAL_STORAGE",
      passwordResetEnabled: types.optional(types.boolean, false),
      registerEnabled: types.optional(types.boolean, false),
      registerStep: types.optional(types.string, "register")
    })
    .actions(self => ({
      afterCreate() {
        //altijd activeren ongeacht of signinlink
        auth.onAuthStateChanged(async user => {
          console.log("onAuthStateChanged", user);
          user && (await self.setMetaData(user.uid, user.email));
          self.setCurrentUser(user);
        });

        if (auth.isSignInWithEmailLink(window.location.href)) {
          getRoot(self).change("$loading", true);
          let email = window.localStorage.getItem("emailForSignIn");
          window.localStorage.removeItem("emailForSignIn");
          if (!email) {
            email = window.prompt("Please provide your email for confirmation");
          }
          if (email) {
            auth
              .signInWithEmailLink(email, window.location.href)
              .then(snapshot => self.setMetaData(snapshot.user.uid, email))
              .catch(function(error) {
                getRoot(self).$alert.add("error", {
                  label: error.code,
                  time: 5000
                });
                console.log(error);
              });
          } else {
            self.setCurrentUser(false);
          }
        }
      },
      setMetaData: flow(function*(uid, email) {
        let groupForMentor = window.localStorage.getItem("group");
        let groupForRespondent = getRoot(self).$router.getURLParameterByName(
          "group"
        );
        let allowedSurvey = window.localStorage.getItem("allowedSurvey");

        let snapshot = yield ref.doc(uid).get();
        if (snapshot.exists) return true;
        else {
          let roles = ["auth"];
          let groupId = groupForRespondent;
          if (groupForMentor) {
            roles.push("mentor");
            getRoot(self).$group.change("selected", {
              _key: "new",
              name: groupForMentor,
              createdBy: uid,
              createdOn: new Date()
            });
            groupForMentor = yield getRoot(self).$group.selected.fsSave();
            getRoot(self).$group.change(
              "selected",
              clone(getRoot(self).$group.list.get(groupForMentor.id))
            );
          }

          let data = {
            roles: roles.join(),
            groups: groupForMentor ? groupForMentor.id : groupForRespondent,
            email,
            createdOn: new Date(),
            allowedSurveys: allowedSurvey
          };
          yield ref.doc(uid).set(data);

          window.localStorage.removeItem("group");
          window.localStorage.removeItem("allowedSurvey");
        }
      }),
      setCurrentUser: flow(function* setCurrentUser(user) {
        const { $router, $survey } = getRoot(self);

        // let cookie = $survey.submittedCookie;
        // let uid = cookie ? cookie.uid : "tempAuth_" + Date.now();
        let group = $router.getURLParameterByName("group");
        // ? $router.getURLParameterByName("group")
        // : cookie
        //   ? cookie.group
        //   : null;

        //is authenticated or more
        if (user && !user.disabled) {
          const meta = yield ref.doc(user.uid).get();
          user = meta.exists
            ? {
                ...user,
                ...meta.data()
              }
            : user;

          self.currentUser = prepareUser(user);
          self.setToList(user.uid, prepareUser(user));
        }

        //is tempauth
        if (!user && group) {
          self.signInAnonymously();
        }

        self.change("status", "done");
      }),
      deleteCurrentUser() {
        self.currentUser = {};
      },
      load: flow(function* load() {
        // getRoot(self).$alert.add("loading", {
        //   loader: "circle"
        // });
        const response = yield _fetch(
          "https://us-central1-barometer-disco.cloudfunctions.net/listUsers",
          false,
          "GET"
        );
        const users = response ? yield response.json() : [];
        if (users.length > 0) {
          const usersMeta = yield ref.get();
          users.forEach(user => {
            usersMeta.forEach(doc => {
              if (user.uid === doc.id)
                user = {
                  ...user,
                  ...doc.data()
                };
            });
            user && self.setToList(user.uid, prepareUser(user));
          });
          getRoot(self).$alert.delete("loading");
        }
      }),
      signIn: flow(function* signIn(email, password) {
        getRoot(self).change("$loading", true);
        self.change("status", "Pending");
        try {
          let user = yield auth.signInWithEmailAndPassword(email, password);
          const userMeta = yield ref.doc(user.uid).get();
          self.setCurrentUser({
            ...user,
            ...userMeta.data()
          });
          self.change("status", "done");
        } catch (error) {
          getRoot(self).change("$loading", false);
          self.change("status", error.code);
        }
      }),
      signInAnonymously: flow(function*() {
        auth.signInAnonymously().catch(function(error) {
          var errorCode = error.code;
          var errorMessage = error.message;
        });
      }),
      signInWithEmail: flow(function* signInWithEmail(email, group) {
        let surveyKey =
          getRoot(self).$survey.selected && getRoot(self).$survey.selected._key;
        var actionCodeSettings = {
          url: `http://${window.location.host}/survey${
            surveyKey && group ? "/share/" + surveyKey : "/view/" + surveyKey
          }`,
          handleCodeInApp: true
        };
        try {
          yield auth.sendSignInLinkToEmail(email, actionCodeSettings);
          email && window.localStorage.setItem("emailForSignIn", email);
          group && window.localStorage.setItem("group", group);
          surveyKey && window.localStorage.setItem("allowedSurvey", surveyKey);
          return true;
        } catch (error) {
          getRoot(self).$alert.add("error", { label: error.code });
        }
      }),
      createUserWithEmailAndPassword: flow(function*(email, password, group) {
        getRoot(self).$alert.add("loading");
        let surveyKey =
          getRoot(self).$survey.selected && getRoot(self).$survey.selected._key;
        try {
          yield auth.createUserWithEmailAndPassword(email, password);
          group && window.localStorage.setItem("group", group);
          surveyKey && window.localStorage.setItem("allowedSurvey", surveyKey);
          getRoot(self).$alert.delete("loading");
          return true;
        } catch (error) {
          getRoot(self).$alert.delete("loading");
          getRoot(self).$alert.add("error", { label: error.code });
        }
      }),
      signOut: flow(function* signOut() {
        yield auth.signOut();
        self.deleteCurrentUser();
        getRoot(self).$router.push(getRoot(self).$config.home);
      }),
      passwordReset: flow(function*(email) {
        getRoot(self).$alert.add("loading");
        try {
          yield auth.sendPasswordResetEmail(email);
          getRoot(self).$alert.delete("loading");
          getRoot(self).$alert.add("success", {
            label:
              getRoot(self).$locale.t("user_password_reset_success") + email
          });
          self.change("passwordResetEnabled", false);
        } catch (error) {
          getRoot(self).$alert.delete("loading");
          getRoot(self).$alert.add("error", { label: error.code });
        }
      })
    }))
    .views(self => ({
      getUserForQuestionKey(key) {
        const user = entries(self.list).find(
          user => String(user[1].questionKey) === String(key)
        );
        return user
          ? {
              key: user[0],
              value: user[1]
            }
          : false;
      },
      getOrganisationForUid(uid) {}
    })),
  actions
);

async function _fetch(url, body = false, method = "POST") {
  try {
    const token = await auth.currentUser.getIdToken();
    let request = {};
    if (method) request["method"] = method;
    if (body) request["body"] = body;
    if (token)
      request["headers"] = {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      };
    return fetch(url, request);
  } catch (error) {
    //console.error(error);
    return false;
  }
}

function prepareUser(user) {
  return {
    email: user.email,
    emailVerified: user.emailVerified,
    allowedSurveys: user.allowedSurveys,
    disabled: user.disabled,
    isAnonymous: user.isAnonymous,
    uid: user.uid,
    roles: user.roles,
    groups: user.groups,
    language: user.language,
    createdOn: user.createdOn
  };
}
