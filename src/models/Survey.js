import { types, getRoot, flow, applySnapshot, clone } from "mobx-state-tree";
import { db } from "../firebase.js";
import { removeInternalProps, slugify } from "../helpers/index.js";
import { actions } from "./_common.js";
import { compare, getCookie, unique } from "../helpers/index.js";
import { values } from "mobx";
import * as dateFns from "date-fns";
import { Answer } from "./Answer";
import XLSX from "xlsx";
import FirestoreDate from "./FirestoreDate";

let ref = db.collection("surveys");
export const Survey = types.compose(
  types
    .model({
      _key: types.identifier,
      _path: types.maybeNull(types.string),
      _activeQuestion: 0,
      _disableWelcome: types.optional(types.boolean, false),
      _answers: types.optional(types.array(Answer), []),
      name: types.string,
      createdBy: types.maybeNull(types.string),
      createdOn: FirestoreDate,
      updatedBy: types.maybeNull(types.string),
      updatedOn: FirestoreDate,
      enabled: types.optional(types.boolean, true),
      disabledMessage: types.optional(
        types.string,
        "Deze enquête is afgesloten"
      ),
      isTemplate: types.optional(types.boolean, false),
      template: types.optional(types.string, ""),
      languages: types.optional(types.string, "nl_BE"),
      interval_period: types.optional(types.string, "none"),
      interval_start: types.maybeNull(FirestoreDate, null)
    })
    .actions(self => ({
      fsSave: flow(function* fsSave() {
        const { $alert, $survey, $locale, $user } = getRoot(self);
        $alert.add("loading");
        self.change("updatedBy", $user.currentUser.uid);
        self.change("updatedOn", new Date());
        let doc = self._key !== "new" ? ref.doc(self._key) : ref.doc();
        yield self._key !== "new"
          ? doc.update(removeInternalProps(self))
          : doc.set(removeInternalProps(self));
        $survey.setToList(doc.id, removeInternalProps(self));
        values($locale.selectedList).forEach(locale => {
          locale.fsSave();
        });
        $alert.delete("loading");
        return doc;
      }),
      fsDelete: flow(function* fsDelete() {
        getRoot(self).$survey.deleteFromList(self._key);
        yield ref.doc(self._key).delete();
      }),
      submitAnswers: flow(function*(e) {
        const { $user, $alert, $router, $validation, $answer } = getRoot(self);
        e.preventDefault();
        $validation.validateForm("survey-" + self._key);
        if ($validation.list.size === 0) {
          $alert.add("loading");
          try {
            yield $answer.saveTempList();
            localStorage.removeItem("barometer-answers");
            //self.setCookie($user.currentUser);
            $alert.delete("loading");
            $router.push("/survey/results/" + self._key);
          } catch (error) {
            console.log(error);
          }
        }
      }),
      // setCookie(user) {
      //   let cookie = `barometer=${user.uid}_${new Date()}_${
      //     user.groups
      //   };Expires=${new Date(self.expireDate).toGMTString()};path=/`; //86400000 * dagen
      //   document.cookie = cookie;
      // },
      changeLanguages(e) {
        let languages = self.languages.split(",");
        let index = languages.indexOf(e.target.value);
        if (index > -1) {
          !e.target.checked && languages.splice(index, 1);
        } else {
          e.target.checked && languages.push(e.target.value);
        }
        self.languages = languages.join();
      },
      prevActiveQuestion(e) {
        e.preventDefault();
        //getRoot(self).$validation.validateForm('survey-' + self._key);
        self._activeQuestion--;
        window.scrollTo(0, 0);
      },
      nextActiveQuestion(e) {
        e.preventDefault();
        //getRoot(self).$validation.validateForm('survey-' + self._key);
        self._activeQuestion++;
        window.scrollTo(0, 0);
      },
      disableQuestionFromLogic(answer) {
        const q = getRoot(self).$question.list.get(answer.questionKey);
        if (q && q.logic) {
          let condition = values(q.logic.conditions)[0];
          let action = q.logic.action.split("_");
          let disabled =
            (condition.question === answer.questionKey &&
              condition.condition === "is" &&
              answer.value === condition.answer) ||
            (condition.question === answer.questionKey &&
              condition.condition === "not" &&
              answer.value !== condition.answer);

          getRoot(self)
            .$question.list.get(action[1])
            .change("enabled", !disabled);

          //remove from templist
          if (disabled) {
            const questionToHide = getRoot(self).$question.list.get(action[1]);
            if (questionToHide.type === "fieldset") {
              const children = self.questions.filter(
                q => q.parentKey === action[1]
              );
              children.forEach(child => {
                if (child.type === "fieldset") {
                  const children = self.questions.filter(
                    q => q.parentKey === child._key
                  );
                  children.forEach(child => {
                    getRoot(self).$answer.deleteFromList(
                      self._key + "_" + child._key,
                      getRoot(self).$answer.tempList
                    );
                  });
                } else {
                  getRoot(self).$answer.deleteFromList(
                    self._key + "_" + child._key,
                    getRoot(self).$answer.tempList
                  );
                }
              });
            } else {
              getRoot(self).$answer.deleteFromList(
                self._key + "_" + action[1],
                getRoot(self).$answer.tempList
              );
            }
          }
        }
      }
    }))
    .views(self => ({
      get expireDate() {
        if (self.availableIntervalPeriods) {
          return self.availableIntervalPeriods[
            self.availableIntervalPeriods.length - 1
          ].endDate;
        } else {
          return new Date(Date.now() + 86400000 * 365);
        }
      },
      get availableIntervalPeriods() {
        let periods = [];
        let time = "Months";
        let amount = 1;
        if (self.interval_period === "none") {
          return false;
        }
        if (self.interval_period === "day") {
          time = "Days";
          amount = 1;
        }
        if (self.interval_period === "week") {
          time = "Weeks";
          amount = 1;
        }
        if (self.interval_period === "month") {
          time = "Months";
          amount = 1;
        }
        if (self.interval_period === "trimester") {
          time = "Months";
          amount = 3;
        }
        if (self.interval_period === "semester") {
          time = "Months";
          amount = 6;
        }
        if (self.interval_period === "year") {
          time = "Years";
          amount = 1;
        }
        let difference = dateFns[`differenceIn${time}`](
          Date.now(),
          self.interval_start
        );

        //if (difference > 0) {
        for (let i = 0; i < difference + 1; i++) {
          let startDate = dateFns[`add${time}`](self.interval_start, i);
          let endDate = dateFns.subDays(
            dateFns[`add${time}`](self.interval_start, i + 1) * amount,
            1
          );
          periods.push({
            startDate,
            endDate
          });
        }
        //} else {
        //   periods.push({
        //     startDate: dateFns.format(Date.now, 'DD/MM/YYYY'),
        //     endDate: dateFns.subDays(dateFns[`add${time}`](self.interval_start, 1) * amount, 1)
        //   });
        // }

        return periods;
      },
      get questionTree() {
        let questions = self.questions;
        let tree = [];

        //tot 3 niveaus
        tree = questions.filter(q => q.enabled).map(q => {
          let children = questions.filter(q1 => q1.parentKey === q._key);
          children = children.map(child => {
            return {
              ...child,
              children: questions
                .filter(q2 => q2.parentKey === child._key)
                .sort(compare("order", "asc"))
            };
          });
          q = { ...q, children };
          return !q.parentKey && q;
        });

        return tree.filter(item => item);
      },
      get questions() {
        return values(getRoot(self).$question.list).sort(
          compare("order", "asc")
        );
      },
      get questionParents() {
        return self.questions.filter(q => !q.parentKey);
      },
      get questionsDisabled() {
        return self.questions.filter(q => !q.enabled);
      },
      get questionsLength() {
        let questions = self.questions;
        return (
          questions.template.filter(q => q.type !== "fieldset").length +
          questions.survey.filter(q => q.type !== "fieldset").length
        );
      },
      get noFieldsets() {
        return self.questions.filter(q => q.type !== "fieldset");
      },
      get fieldsets() {
        return self.questions.filter(q => q.type === "fieldset");
      },
      get answers() {
        return values(self._answers);
      },
      get respondents() {
        const { $user } = getRoot(self);
        const answers = $user.currentUser.isCreator
          ? self.answers
          : self.answers.filter(
              answer => answer.groupKey === $user.currentUser.groups
            );
        return unique(answers, "createdBy");
      },
      get answersForCurrentUID() {
        return self.answers.filter(
          answer => answer.createdBy === getRoot(self).$user.currentUser.uid
        );
      },
      get answersPerUser() {
        return self.answers.reduce((acc, elem) => {
          if (!acc[elem.createdBy]) acc[elem.createdBy] = [];
          acc[elem.createdBy].push(elem.toJSON());
          return acc;
        }, {});
      },
      get resultsToXls() {
        const { $config } = getRoot(self);
        let users = {};

        const a = getRoot(self).$question.list.get("scIwRxe3sUfDFFJofsEc");
        const a1 = a.children.find(
          child => child._key === "cw3IiH2F8Kv6JRGJyPH0"
        );
        const a2 = a.children.find(
          child => child._key === "4Nf12jRi56OGzAQ6eYIo"
        );

        const b = getRoot(self).$question.list.get("viaKmvn4KBxs2aLh2I0x");
        const b1 = b.children[0];
        const b2 = b.children[1];
        const b3 = b.children[2];
        const b4 = b.children[3];

        console.log(
          "a.children :",
          b.children[0]._key,
          b.children[1]._key,
          b.children[2]._key
        );

        let results = Object.keys(self.answersPerUser).map(uid => {
          let user = {};
          let email = self.answersPerUser[uid].find(
            a => a.questionKey === "ufxnATWEGy3EA4G47iIj"
          );
          let geboortedatum = self.answersPerUser[uid].find(
            a => a.questionKey === "cNH0Fp7jPpCK5hGu2fJe"
          );
          let geslacht = self.answersPerUser[uid].find(
            a => a.questionKey === "g5WJafXpe8lgezcu1rZB"
          );
          let onderwijsniveau = self.answersPerUser[uid].find(
            a => a.questionKey === "95fu7aaidSyHP6ABrzpt"
          );
          let ervaring_jaren = self.answersPerUser[uid].find(
            a => a.questionKey === "vHyStX0NWGrGfo543anY"
          );
          let ervaring_collegas = self.answersPerUser[uid].find(
            a => a.questionKey === "6m0YRT1oaO5sTgogOtvm"
          );

          user.uid = uid;
          user.email = email ? email.value : "";
          user.geboortedatum = geboortedatum ? geboortedatum.value : "";
          user.geslacht = geslacht ? geslacht.value : 999;
          user.onderwijsniveau = onderwijsniveau ? onderwijsniveau.value : 999;
          user.ervaring_jaren = ervaring_jaren ? ervaring_jaren.value : 999;
          user.ervaring_collegas = ervaring_collegas
            ? ervaring_collegas.value
            : 999;

          a1.children.forEach((q, i) => {
            user["1_a1_" + (i + 1)] = q.getAnswerValuesForIndividual(uid)[0];
          });

          a2.children.forEach((q, i) => {
            user["1_a2_" + (i + 1)] = q.getAnswerValuesForIndividual(uid)[0];
          });

          b1.children.forEach((q, i) => {
            user["2_b1_" + (i + 1)] = q.getAnswerValuesForIndividual(uid)[0];
          });

          b2.children.forEach((q, i) => {
            user["2_b2_" + (i + 1)] = q.getAnswerValuesForIndividual(uid)[0];
          });

          b3.children.forEach((q, i) => {
            user["2_b3_" + (i + 1)] = q.getAnswerValuesForIndividual(uid)[0];
          });

          b4.children.forEach((q, i) => {
            user["2_b4_" + (i + 1)] = q.getAnswerValuesForIndividual(uid)[0];
          });

          a1.children.forEach((child, i) => {
            let index = $config.results[0].branches.findIndex(
              branch => branch.match.indexOf(i) > -1
            );
            let answersIndividual = child.getAnswerValuesForIndividual(uid);
            if (index > -1) {
              let array = [];
              array.push(...answersIndividual);
              user["3_a_" + slugify($config.results[0].branches[index].label)] =
                [...answersIndividual].reduce((a, b) => Number(a) + Number(b)) /
                [...answersIndividual].length;
            }
          });

          user["4_a2"] = getRoot(self)
            .$question.list.get(a2._key)
            .getAverageAnswerValue({
              by: "uid",
              value: uid
            });

          user["4_b1"] = getRoot(self)
            .$question.list.get(b1._key)
            .getAverageAnswerValue({
              by: "uid",
              value: uid
            });

          user["4_b2"] = getRoot(self)
            .$question.list.get(b2._key)
            .getAverageAnswerValue({
              by: "uid",
              value: uid
            });

          user["4_b3"] = getRoot(self)
            .$question.list.get(b3._key)
            .getAverageAnswerValue({
              by: "uid",
              value: uid
            });

          user["4_b4"] = getRoot(self)
            .$question.list.get(b4._key)
            .getAverageAnswerValue({
              by: "uid",
              value: uid
            });

          return user;
        });

        console.log("results :", results);

        // let answers = self.answers.forEach(a => {

        //   return users.
        // })
        // let answers = self.questions.map(q => {
        //   return q.answers.map(a => {
        //     let newA = {};
        //     const survey = getRoot(self).$survey.list.get(a.surveyKey);
        //     const group = getRoot(self).$group.list.get(a.groupKey);
        //     newA.createdBy = a.createdBy;
        //     newA.createdOn = a.created;
        //     newA.value = a.value;
        //     newA.surveyKey = a.surveyKey;
        //     newA.surveyName = survey && survey.name;
        //     newA.groupKey = a.groupKey;
        //     newA.groupName = group && group.name;
        //     newA.questionKey = a.questionKey;
        //     newA.questionName = getRoot(self).$locale.t(a.questionKey);
        //     if (q.parentKey) {
        //       newA.parentKey = q.parentKey;
        //       newA.parentName = getRoot(self).$locale.t(q.parentKey);
        //     }
        //     return a.value && newA;
        //   });
        // });
        // answers = [].concat.apply([], answers.filter(a => a.length > 0));

        const wb = XLSX.utils.book_new();
        const ws = XLSX.utils.json_to_sheet(results);
        XLSX.utils.book_append_sheet(wb, ws, "Antwoorden per gebruiker");
        XLSX.writeFile(
          wb,
          "barometer_antwoorden_" +
            self._key +
            "_" +
            dateFns.format(Date.now(), "DDMMYY") +
            ".xlsx"
        );
      },
      getHighestOrderValue(parentKey) {
        if (parentKey) {
          let value = 0;
          self.questionTree.forEach(q => {
            if (q._key === parentKey && q.children[q.children.length - 1]) {
              value = q.children[q.children.length - 1].order + 1;
            } else {
              value = 0;
            }
          });
          return value;
        } else {
          return self.questionTree[self.questionTree.length - 1]
            ? self.questionTree[self.questionTree.length - 1].order + 1
            : 0;
        }
      },
      getActiveQuestionKey(activeQuestionIndex) {
        let activeQuestion = self.questionParents.filter(q => q.enabled)[
          activeQuestionIndex
        ];
        if (!activeQuestion) return false;
        if (activeQuestion.type === "hidden")
          activeQuestion = self.questionParents[activeQuestionIndex];
        let key = activeQuestion._key;
        if (activeQuestion.logic) {
          let match = false;
          activeQuestion.logic.conditions.forEach(condition => {
            let question = getRoot(self).$question.list.get(condition.question);
            if (
              question.answerTemp &&
              condition.answer === question.answerTemp.value
            ) {
              match = true;
            }
          });
          if (match) {
            let action = activeQuestion.logic.action.split("_");
            if (action[0] === "goto") key = action[1];
          }
        }
        return key;
      }
    })),
  actions
);

export const SurveyStore = types.compose(
  types
    .model("SurveyStore", {
      list: types.optional(types.map(Survey), {}),
      status: types.optional(types.string, "pending"),
      selected: types.maybeNull(Survey),
      activeIntervalPeriod: types.maybeNull(types.Date)
    })
    .actions(self => {
      return {
        setActiveIntervalPeriod(date) {
          self.activeIntervalPeriod = date
            ? new Date(date)
            : self.selected.availableIntervalPeriods.pop().startDate;
        },
        setSelected(survey) {
          const { $answer } = getRoot(self);
          self.selected = survey;
          let tempAnswers = JSON.parse(
            localStorage.getItem("barometer-answers")
          );
          tempAnswers && applySnapshot($answer.tempList, tempAnswers);
          self.selected &&
            tempAnswers &&
            Object.keys(tempAnswers).forEach(key => {
              self.selected.disableQuestionFromLogic(tempAnswers[key]);
            });
        },
        load: flow(function* load(id) {
          const { $alert, $user, $question, $router } = getRoot(self);
          const currentUser = $user.currentUser;

          !getRoot(self).$loading && $alert.add("loading");
          console.log("> load survey", id, currentUser.isMentor, self.selected);

          if (!id) {
            try {
              //templates
              let snapshot = yield ref
                .where("enabled", "==", true)
                .orderBy("createdOn", "desc")
                .get();
              snapshot.forEach(async doc => {
                self.setToList(doc.id, doc.data());
                let answers = await doc.ref.collection("answers").get();
                answers = answers.docs.map(doc => doc.data());
                applySnapshot(self.list.get(doc.id)._answers, answers);
              });
              $alert.delete("loading");
            } catch (error) {
              console.log("error", error);
              $alert.delete("loading");
              $alert.add("error", { label: error.code });
            }
            self.setProp("status", "done");
          } else {
            try {
              const snapshot = yield ref.doc(id).get();
              if (snapshot.exists) {
                self.setToList(snapshot.id, snapshot.data());
                self.setSelected(clone(self.list.get(snapshot.id)));
                if (
                  snapshot.data().interval_period !== "none" &&
                  !self.activeIntervalPeriod
                ) {
                  self.setActiveIntervalPeriod();
                }

                //load questions
                yield $question.load(snapshot.ref.collection("questions"));
                //load answers
                if (
                  $router.mode === "results" ||
                  $router.mode === "edit" ||
                  $router.mode === "view"
                ) {
                  let answers = snapshot.ref.collection("answers");
                  const period =
                    self.selected.availableIntervalPeriods &&
                    self.selected.availableIntervalPeriods.find(date =>
                      dateFns.isEqual(date.startDate, self.activeIntervalPeriod)
                    );
                  if (
                    ($user.currentUser.isAnonymous ||
                      $user.currentUser.isAuth) &&
                    !$user.currentUser.isCreator
                  ) {
                    if ($user.currentUser.groupKey) {
                      if (period) {
                        answers = yield answers
                          .where("groupKey", "==", $user.currentUser.groupKey)
                          .where("createdOn", ">=", period.startDate)
                          .where("createdOn", "<=", period.endDate)
                          .get();
                      } else {
                        answers = yield answers
                          .where("groupKey", "==", $user.currentUser.groupKey)
                          .get();
                      }
                    } else {
                      if (period) {
                        answers = yield answers
                          .where("createdOn", ">=", new Date(period.startDate))
                          .where("createdOn", "<=", new Date(period.endDate))
                          .get();
                      } else {
                        answers = yield answers.get();
                      }
                    }
                  } else {
                    if (period) {
                      answers = yield answers
                        .where("createdOn", ">=", period.startDate)
                        .where("createdOn", "<=", period.endDate)
                        .get();
                    } else {
                      answers = yield answers.get();
                    }
                  }
                  answers = answers.docs.map(doc => doc.data());
                  console.log("answers :", answers);
                  if (self.selected) {
                    applySnapshot(self.selected._answers, answers);
                  } else {
                    applySnapshot(self.list.get(snapshot.id)._answers, answers);
                  }
                }
                self.setProp("status", "done");
              } else {
                $alert.add("error", { label: "Survey ID not found" });
              }
            } catch (error) {
              $alert.add("error", { label: "error" });
            }
            $alert.delete("loading");
          }
        })
      };
    })

    .views(self => ({
      // get submittedCookie() {
      //   let cookie = getCookie("barometer");
      //   if (cookie) {
      //     cookie = cookie.split("_");
      //     console.log("Cookie: ", cookie);
      //     return cookie.length > 0
      //       ? {
      //           uid: cookie[0] + "_" + cookie[1],
      //           created: cookie[2],
      //           group: cookie[3]
      //         }
      //       : false;
      //   } else {
      //     return false;
      //   }
      // },
      get templates() {
        return values(self.list).filter(item => item.isTemplate);
      }
    })),
  actions
);
