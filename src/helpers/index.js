//import "./array.js";

export function getDataUri(url, callback) {
  var image = new Image();

  image.onload = function() {
    var canvas = document.createElement('canvas');
    canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
    canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size
    canvas.getContext('2d').drawImage(this, 0, 0);

    // Get raw image data
    callback(canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''));

    // ... or get as Data URI
    callback(canvas.toDataURL('image/png'));
  };

  image.src = url;
}

export function getCookie(name) {
  const match = document.cookie.match(new RegExp(name + '=([^;]+)'));
  return match && match[1];
}

// export function unique(array, key) {
//   return array.reduce(function(carry, item) {
//     if (item[key] && !~carry.indexOf(item[key])) carry.push(item[key]);
//     return carry;
//   }, []);
// }

export function unique(array, propertyName) {
  return array.filter((e, i) => array.findIndex(a => a[propertyName] === e[propertyName]) === i);
}

export function URLToArray(url) {
  url = url || window.location.search;
  var request = {};
  var pairs = url.substring(url.indexOf('?') + 1).split('&');
  for (var i = 0; i < pairs.length; i++) {
    if (!pairs[i]) continue;
    var pair = pairs[i].split('=');
    request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
  }
  return request;
}

export function getURLParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

export function slugify(text) {
  try {
    return text
      .toString()
      .toLowerCase()
      .replace(/\s+/g, '-') // Replace spaces with -
      .replace(/[^\w-]+/g, '') // Remove all non-word chars
      .replace(/--+/g, '-') // Replace multiple - with single -
      .replace(/^-+/, '') // Trim - from start of text
      .replace(/-+$/, ''); // Trim - from end of text
  } catch (e) {
    console.info('Slugify', e);
  }
}

export const pipe = (f, g) => (...args) => g(f(...args));

export const flatten = array => {
  array.forEach(item => {
    Object.keys(item).forEach(itemKey => {
      if (Array.isArray(item[itemKey])) {
        item[itemKey].forEach(subitem => {
          item['_' + subitem.key] = subitem.value;
        });
      }
    });
  });
  return array;
};

export const compare = (key, order = 'asc') => {
  return function(a, b) {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      return 0;
    }

    const varA = typeof a[key] === 'string' ? a[key].toUpperCase() : a[key];
    const varB = typeof b[key] === 'string' ? b[key].toUpperCase() : b[key];

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return order === 'desc' ? comparison * -1 : comparison;
  };
};



export const removeInternalProps = object => {
  const reduced = {};
  Object.keys(object).forEach(key => {
    if (key.substr(0, 1) !== '_') reduced[key] = object[key];
  });
  return reduced;
};
